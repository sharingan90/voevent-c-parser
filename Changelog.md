# Changelog for the voevent-c-parser library

## Janaury 2023
- Release [1.1.6](../../tags/v1.1.6) (6th January 2023)
- Add two functions and make voe-config scruipt executable.

## March 2021
- Release [1.1.4](../../tags/v1.1.4) (12th March 2021)
- Add the possibility to parse XML files from memory buffers

## November 2020
- Release [1.1.3](../../tags/v1.1.3) (3rd November 2020)

## September - October 2020
- Trigger number is returned as long long int by get_trigger_number
  function, so that it can contain all trigger numbers from GCN VOEvents,
  in particular those from MAXI Known Notices
- Add possibility to build VOEvents: for the moment the API is not complete,
  but it allows to build a full VOEvent with the main elements
- Add definition of UCD keyword

## August 2020
- Release [1.1.2](../../tags/v1.1.2) (13th August 2020)
- Print main elements only when they are actually present in the VOEvent message.
- Solve bug introduced in v1.1.1: add check of description text before string <br>
  comparison; it causes a crash if the Description element is missing.
- Add FRB VOEvent example
- Release [1.1.1](../../tags/v1.1.1) (11th August 2020)
- Update README
- Added this Changelog
- Add creation of documentation with Doxygen via CMake
- Fix to printing functions for Description, Reference and Citations elements
- Release [1.1](../../tags/v1.1) (10th August 2020)
- Updates of documentation
- Added usage notes to README
- Added installation notes
- Create shared library with version numbers and symbolic links to it
- Do not call xmlSchemaCleanupTypes when validating, it is not thread-safe. <br>
  It is called by xmlCleanupParser anyway. <br>
  See https://stackoverflow.com/questions/13470166/add-additional-xsd-schemas-with-libxml2

## July 2020
- Added support for threads in compilation
- Complete and update CMake configuration file

## July 2019
- Added validation of Transport messages
- Added parsing of Transport messages, needed for iamalive, authenticate and ack/nack <br>
  messages in the VOEvent Transport Protocol
- Added GCN-specific utility functions

## December 2018
- Release [1.0](../../tags/v1.0) (3rd December 2018)
- Added CMake-based compilation

## November 2018
- Add functions to print VOEvent elements
- Added generic utility functions
- Added functions to free the memory
- Initial commit: library with parsing functions and small program to test functionalities
