# README FOR VOEVENT-C-PARSER LIBRARY

`voevent-c-parser` is a parser for VOEvents written in C using libxml2 (http://xmlsoft.org/ and https://gitlab.gnome.org/GNOME/libxml2). It uses simple structures which are filled with the VOEvent information extracted from the XML file. The user can extract the needed information with some general utility functions provided by the library. Also, there are some utility functions specific for VOEvents distributed by the Gamma-ray Coordinates Network (GCN, https://gcn.gsfc.nasa.gov/). The library also provides validation against the VOEvent and Transport schemas, which are shipped with the library.

## LATEST RELEASE

The latest stable release of `voevent-c-parser` is [v1.1.6](../../releases/v1.1.6).

## INSTALLATION

Before installing, you should get the latest stable release (v1.1.6) via `Git`:

```bash
git clone https://gitlab.com/aleberti/voevent-c-parser.git
cd voevent-c-parser
git checkout v1.1.6
```

or with `wget`:

```bash
wget https://gitlab.com/aleberti/voevent-c-parser/-/archive/v1.1.6/voevent-c-parser-v1.1.6.tar.gz
```

In the second case, you should also decompress the archive using `tar`:

```bash
tar -xzf voevent-c-parser-v1.1.6.tar.gz
```

The installation uses `CMake`, with minimum required version equal to 3.9. If you do not have `CMake` installed, please refer to the website https://cmake.org/ to know how to retrieve `CMake` and install a suitable version.

Now you can proceed with the installation. You have now a directory called `voevent-c-parser` (or `voevent-c-parser-v1.1.6` if you used `wget`). You can install with the following commands:

```bash
cd voevent-c-parser
mkdir build
cd build
cmake -DUSE_THREADS=true -DCMAKE_INSTALL_PREFIX=./ ../
# install in the build directory; change CMAKE_INSTALL_PREFIX
# to install somewhere else, probably more suitable :)
make
# use also 'make -j n', with 'n' number of cores, to use more
# core during compilation. It should not be needed since this
# library is lightweight
make install
```

The `USE_THREADS` option tells `cmake` to search for the thread library, and link `voevent-c-parser` with it. The `CMAKE_INSTALL_PREFIX` option tells where the binaries, library and auxiliary files will be installed when calling `make install`.

After successfull compilation and installation, the following files will be in the `CMAKE_INSTALL_PREFIX` directory:

* `CMAKE_INSTALL_PREFIX/lib/libvoevent.so`: the shared library of `voevent-c-parser`, to which you can link your application using the library
* `CMAKE_INSTALL_PREFIX/include/voevent.h`: the header file containing the prototypes for the functions defined in `voevent-c-parser`. You need to include this header file in the source/header files using `voevent-c-parser` functions
* `CMAKE_INSTALL_PREFIX/include/voevent_version.h`: header file containing the major and minor version numbers of the library
* `CMAKE_INSTALL_PREFIX/voevent_examples`: contains few examples of VOEvents
* `CMAKE_INSTALL_PREFIX/bin/parse_voevent` and `CMAKE_INSTALL_PREFIX/bin/parse_transport`: two executables to test the library and providing a guideline on how to use the library. You can add the `CMAKE_INSTALL_PREFIX/bin` directory to your `PATH` so that you can use the two executables from anywhere in your system
* `CMAKE_INSTALL_PREFIX/bin/parse_voevent_memory` and `CMAKE_INSTALL_PREFIX/bin/parse_transport_memory`: they are similar to `parse_voevent` and `parse_transport`, but the XML resides in memory, so that it does not need an input file.
* `CMAKE_INSTALL_PREFIX/bin/voe-config`: a bash script which can be used to get preprocessor/compiler and linking flags when building an application using `voevent-c-parser` (see below).

If you specify the option `-DSCHEMAS_DIR`, the schema files in the `schemas` directory will be copied to the directory pointed by `-DSCHEMAS_DIR`. If not specified, they will be put in the `CMAKE_INSTALL_PREFIX/schemas` directory.

## USAGE

Two examples of how the library should be used are given in the `CMAKE_INSTALL_PREFIX/bin` directory. The schema files must be in the current working directory.

For example, to use the `parse_voevent` executable:

```bash
parse_voevent /path/to/voevent_examples/swift_bat_grb_pos.xml
```

If everything works fine, the file will be valid against the VOEvent schema and the content of the file with some formatting will be shown in the standard output.

To parse a Transport message, `parse_transport` should be used insead:

```bash
parse_transport /path/to/voevent_examples/iamalive.xml
```

If you are building an application using `voevent-c-parser`, you should include the header file `voevent.h` and link it against the shared library created during the compilation of `voevent-c-parser`, called `libvoevent.so`. To get preprocessor/compiler and linking flags, a script called `voe-config` is provided. If you installed `voevent-c-parser` in a directory in your `PATH`, to get the preprocessor/compiler flags, invoke `voe-config` as:

```bash
voe-config --cflags
```

To get linking flags, run:

```bash
voe-config --libs
```

For other options, run `voe-config --help`.

## GENERATING DOCUMENTATION

The documentation of the library is created using Doxygen (http://www.doxygen.org/). CMake will try to find the doxygen executable and requires to have also the `dot` utility from Graphviz (http://graphviz.org/). If found, a new target called `doc` will be available. To create the documentation the you can just run:

```bash
make doc
```

and the documentation (both HTML and LaTeX) will be put in the `CMAKE_INSTALL_PREFIX/doc` directory.

You can create the documentation directly using doxygen. A configuration file, `voevent-c-parser_doxygen.cfg`, is provided. To create the documentation just run:

```bash
doxygen voevent-c-parser_doxygen.cfg
```

A folder called `doc` will be created in the current working directory.

## LATEST CHANGES

You can find the list of changes of the library in the [Changelog](Changelog.md).

## ISSUE AND BUGS

If you find issues or bugs, please report them by sending an email to [alessioberti90+voevent@gmail.com](mailto:alessioberti90+voevent@gmail.com). Please provide the following information in the email:
- the OS you are using (version, distro)
- the version of the library you are using
- a minimal code snippet which reproduces the error (if not too long, just put it as text, do not attach a file to the mail)
- a log file containing the error message

I will try to solve it as soon as I can, if it can be reproduced.

## DEVELOPER

Alessio Berti <br>
mail: [alessioberti90@gmail.com](mailto:alessioberti90@gmail.com)
