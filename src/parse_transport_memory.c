/**
 * @file parse_transport_memory.c
 * @brief Source file to test the voevent reader/parser.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlschemastypes.h>
#include "voevent.h"

/**
 * @brief      Example program to parse VOEvents of Transport type
 *
 *
 * @param  argc   Number of command line arguments
 * @param  argv   Array with command line arguments
 *
 * @return     0 if no errors occured, 1 otherwise
 */
int main(int argc, char const *argv[])
{
    if(argc < 1)
    {
        fprintf(stderr, "You did not provide enough arguments. Exiting.\n");
        return 1;
    }

    #ifdef LIBXML_THREAD_ENABLED
        printf("Thread support enabled\n");
    #endif

    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used. Calls xmlInitParser() internally
    */
    LIBXML_TEST_VERSION

    char *filename = (char *) malloc(strlen(argv[1])+1);
    strcpy(filename, argv[1]);

    FILE *fp;
    long lSize;
    char *buffer;

    fp = fopen (filename, "rb" );
    if( !fp ) perror(filename),exit(1);

    fseek( fp , 0L , SEEK_END);
    lSize = ftell( fp );
    rewind( fp );

    /* allocate memory for entire content */
    buffer = calloc( 1, lSize+1 );
    if( !buffer ) fclose(fp),fputs("memory alloc fails",stderr),exit(1);

    /* copy the file into the buffer */
    if( 1!=fread( buffer , lSize, 1 , fp) ){
        fclose(fp);
        free(buffer);
        fputs("entire read fails",stderr);
        exit(1);
    }

    struct Transport transport = {0};
    int parsed = parse_transport_xmlmemory(buffer, lSize+1, &transport);
    if (parsed != 0){
        free_transport(&transport);
        free(filename);
        xmlCleanupParser();
        fclose(fp);
        free(buffer);
        return 1;
    }
    double sod = get_transport_sod(&transport);
    printf("SOD: %.2f\n", sod);
    free_transport(&transport);
    free(filename);
    xmlCleanupParser();
    fclose(fp);
    free(buffer);
    return 0;
}
