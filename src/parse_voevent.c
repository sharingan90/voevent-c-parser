/**
 * @file parse_voevent.c
 * @brief Source file to test the voevent reader/parser.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlschemastypes.h>
#include "voevent.h"
//#include "voevent_version.h"

/**
 * @brief      Example program to parse VOEvents
 *
 *
 * @param  argc   Number of command line arguments
 * @param  argv   Array with command line arguments
 *
 * @return     0 if no errors occured, 1 otherwise
 */
int main(int argc, char const *argv[])
{
    if(argc < 1)
    {
        fprintf(stderr, "You did not provide enough arguments. Exiting.\n");
        return 1;
    }

    //printf("You are using VOEVENT PARSER LIBRARY VERSION %d.%d\n", voevent_VERSION_MAJOR, voevent_VERSION_MINOR);

    #ifdef LIBXML_THREAD_ENABLED
        printf("Thread support enabled\n");
    #endif

    /*
     * This initializes the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used. Calls xmlInitParser() internally
    */
    //xmlInitParser();
    LIBXML_TEST_VERSION

    char *filename = (char *) malloc(strlen(argv[1])+1);
    strcpy(filename, argv[1]);

    struct VOEvent voevent = {0};
    int parsed = parse_voevent_xmlfile(filename, &voevent);
    if (parsed != 0){
        free_voevent(&voevent);
        free(filename);
        xmlCleanupParser();
        return 1;
    }
    printf("\n");
    //struct VOEvent_Author *author = get_author(&voevent);
    print_voevent(&voevent);
    struct VOEvent_Param *Def_NOT_a_GRB = get_param_by_name(&voevent, "Def_NOT_a_GRB");
    if(Def_NOT_a_GRB)
        printf("Def_NOT_a_GRB : %s\n", Def_NOT_a_GRB->value);
    int packet_type = get_packet_type(&voevent);
    printf("Packet type: %d\n", packet_type);
    double ra = get_ra(&voevent);
    double dec = get_dec(&voevent);
    double errorbox = get_coord_error(&voevent);
    printf("(RA, Dec) : (%.2f, %.2f); errorbox: %.2f\n", ra, dec, errorbox);
    struct VOEvent_Group *group1 = get_group_by_index(&voevent, 2);
    if(group1)
        print_voevent_group(group1);
    struct tm trigger_time = {0};
    struct tm packet_time = {0};
    get_trigger_time(&voevent, &trigger_time);
    get_packet_time(&voevent, &packet_time);
    printf("%s", asctime(&trigger_time));
    printf("%s", asctime(&packet_time));
    printf("Alert %s a test.\n", is_test(&voevent) ? "is" : "is not");
    printf("Alert %s an observation.\n", is_observation(&voevent) ? "is" : "is not");
    free_voevent(&voevent);
    free(filename);
    xmlCleanupParser();
    return 0;
}
