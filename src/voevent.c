/**
 * @file voevent.c
 * @brief Source file for the voevent reader/parser.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlschemastypes.h>
#include <libxml/xpath.h>
#include "voevent.h"

const char *VOEventSchemaFile   = "VOEvent-v2.0.xsd";    ///< Name of the file containing the XML schema for VOEvents
const char *TransportSchemaFile = "Transport-v1.1.xsd";  ///< Name of the file containing the XML schema for Transport messages

/**
 * @brief      Validate a VOEvent contained in a XML file against the VOEvent 2.0 XML Schema
 *
 * Code example from http://knol2share.blogspot.com/2009/05/validate-xml-against-xsd-in-c.html.
 * The code example is an extract of xmllint.c, belonging to the libxml2 library.
 *
 * @param[in]  XMLFilename  The VOEvent XML filename
 * @param[in]  XSDFilename  The VOEvent schema filename
 *
 * @return     0 if valid VOEvent, 1 otherwise
 */
int validate_voevent_xmlfile(const char *XMLFilename, const char *XSDFilename){

    xmlDocPtr doc;
    xmlSchemaPtr schema;
    xmlSchemaParserCtxtPtr ctxt;
    int ret;

    xmlLineNumbersDefault(1);

    ctxt = xmlSchemaNewParserCtxt(XSDFilename);

    xmlSchemaSetParserErrors(ctxt, (xmlSchemaValidityErrorFunc) fprintf, (xmlSchemaValidityWarningFunc) fprintf, stderr);
    schema = xmlSchemaParse(ctxt);
    xmlSchemaFreeParserCtxt(ctxt);
    ctxt = NULL;
    if(schema == NULL){
        fprintf(stderr, "An error occurred creating Schema context for the validation\n");
        ret = 1;
        //xmlSchemaCleanupTypes();
        xmlMemoryDump();
        return ret;
    }
    //xmlSchemaDump(stdout, schema); //To print schema dump

    doc = xmlReadFile(XMLFilename, NULL, 0);

    if (doc == NULL){
        fprintf(stderr, "Could not parse %s\n", XMLFilename);
        ret = 1;
    }
    else
    {
        xmlSchemaValidCtxtPtr ctxt_schema_valid;

        ctxt_schema_valid = xmlSchemaNewValidCtxt(schema);
        xmlSchemaSetValidErrors(ctxt_schema_valid, (xmlSchemaValidityErrorFunc) fprintf, (xmlSchemaValidityWarningFunc) fprintf, stderr);
        ret = xmlSchemaValidateDoc(ctxt_schema_valid, doc);
        if (ret == 0){
            printf("%s validates\n", XMLFilename);
        }
        else if (ret > 0)
        {
            printf("%s fails to validate\n", XMLFilename);
        }
        else
        {
            printf("%s validation generated an internal error\n", XMLFilename);
        }

        xmlSchemaFreeValidCtxt(ctxt_schema_valid);
        ctxt_schema_valid = NULL;
        xmlFreeDoc(doc);
        doc = NULL;
    }

    // free the resource
    if(schema != NULL){
        xmlSchemaFree(schema);
        schema = NULL;
    }

    //xmlSchemaCleanupTypes();
    xmlMemoryDump();

    return ret;
}

/**
 * @brief      Validate a VOEvent contained in a string buffer against the VOEvent 2.0 XML Schema
 *
 * Code example from http://knol2share.blogspot.com/2009/05/validate-xml-against-xsd-in-c.html.
 * The code example is an extract of xmllint.c, belonging to the libxml2 library.
 *
 * @param[in]  XMLbuffer    The string buffer
 * @param[in]  size         Size of the string buffer
 * @param[in]  XSDFilename  The VOEvent schema filename
 *
 * @return     0 if valid VOEvent, 1 otherwise
 */
int validate_voevent_xmlmemory(const char *XMLbuffer, const long int size, const char *XSDFilename){

    xmlDocPtr doc;
    xmlSchemaPtr schema;
    xmlSchemaParserCtxtPtr ctxt;
    int ret;

    xmlLineNumbersDefault(1);

    ctxt = xmlSchemaNewParserCtxt(XSDFilename);

    xmlSchemaSetParserErrors(ctxt, (xmlSchemaValidityErrorFunc) fprintf, (xmlSchemaValidityWarningFunc) fprintf, stderr);
    schema = xmlSchemaParse(ctxt);
    xmlSchemaFreeParserCtxt(ctxt);
    ctxt = NULL;
    if(schema == NULL){
        fprintf(stderr, "An error occurred creating Schema context for the validation\n");
        ret = 1;
        //xmlSchemaCleanupTypes();
        xmlMemoryDump();
        return ret;
    }
    //xmlSchemaDump(stdout, schema); //To print schema dump

    doc = xmlReadMemory(XMLbuffer, size, NULL, NULL, 0);

    if (doc == NULL){
        fprintf(stderr, "Could not parse string buffer\n");
        ret = 1;
    }
    else
    {
        xmlSchemaValidCtxtPtr ctxt_schema_valid;

        ctxt_schema_valid = xmlSchemaNewValidCtxt(schema);
        xmlSchemaSetValidErrors(ctxt_schema_valid, (xmlSchemaValidityErrorFunc) fprintf, (xmlSchemaValidityWarningFunc) fprintf, stderr);
        ret = xmlSchemaValidateDoc(ctxt_schema_valid, doc);
        if (ret == 0){
            printf("VOEvent validates\n");
        }
        else if (ret > 0)
        {
            printf("VOEvent fails to validate\n");
        }
        else
        {
            printf("VOEvent validation generated an internal error\n");
        }

        xmlSchemaFreeValidCtxt(ctxt_schema_valid);
        ctxt_schema_valid = NULL;
        xmlFreeDoc(doc);
        doc = NULL;
    }

    // free the resource
    if(schema != NULL){
        xmlSchemaFree(schema);
        schema = NULL;
    }

    //xmlSchemaCleanupTypes();
    xmlMemoryDump();

    return ret;
}


/**
 * @brief      Get the set of nodes corresponding to the given XPath.
 *
 * @param[in]  doc    The pointer to the structure containing the tree of the parsed document
 * @param[in]  xpath  The XPath expression
 *
 * @return     An xmlXPathObjectPtr object containing the set of nodes corresponding to the given XPath.
 */
xmlXPathObjectPtr getnodeset(xmlDocPtr doc, xmlChar *xpath){
    
    if(doc == NULL || xpath == NULL){
        return NULL;
    }

    xmlXPathContextPtr context = xmlXPathNewContext(doc);
    if (context == NULL) {
        printf("Error in xmlXPathNewContext\n");
        return NULL;
    }
    xmlXPathObjectPtr result = xmlXPathEvalExpression(xpath, context);
    xmlXPathFreeContext(context);
    if (result == NULL) {
        printf("Error in xmlXPathEvalExpression\n");
        return NULL;
    }
    if(xmlXPathNodeSetIsEmpty(result->nodesetval)){
        xmlXPathFreeObject(result);
        //printf("No result\n");
        return NULL;
    }
    return result;
}

/*
better to parse the VOEvent in chunks: first the root node, then who, then what and so on
*/

/**
 * @brief      Parse the root element of a VOEvent
 *
 * @param[in]  doc           The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns            XML Namespace
 * @param[in]  root_node     The node corresponding to the root element of the VOEvent
 * @param      voevent_root  A pointer to a VOEvent_root structure
 */
void parse_root(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr root_node, struct VOEvent_root *voevent_root) {

    // get VOEvent version, role and ivorn. They MUST be there, otherwise the document would not
    // be valid. Cleanup should be performed somewhere since xmlGetProp allocates memory.

    voevent_root->version = xmlGetProp(root_node, (const xmlChar *) "version");
    voevent_root->role    = xmlGetProp(root_node, (const xmlChar *) "role");
    voevent_root->ivorn   = xmlGetProp(root_node, (const xmlChar *) "ivorn");

    return;
}

/**
 * @brief      Parse a Reference element of a VOEvent
 *
 * @param[in]  doc                The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                 XML Namespace
 * @param[in]  ref_node           The node corresponding to the Reference element
 * @param      voevent_reference  A pointer to a VOEvent_Reference structure
 */
void parse_reference(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr ref_node, struct VOEvent_Reference *voevent_reference) {
    xmlNodePtr cur;

    // set the elements to NULL in order to not have problems when Freeing the structure
    voevent_reference->uri      = NULL;
    voevent_reference->type     = NULL;
    voevent_reference->mimetype = NULL;
    voevent_reference->meaning  = NULL;

    /* We don't care what the top level element name is */
    cur = ref_node->xmlChildrenNode;
    while (cur != NULL) {
        if (!xmlStrcmp(cur->name, (const xmlChar *) "uri")){
            voevent_reference->uri = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "type")){
            voevent_reference->type = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "mimetype")){
            voevent_reference->mimetype = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "meaning")){
            voevent_reference->meaning = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        cur = cur->next;
    }

    return;
}

/**
 * @brief      Parse an Author element of a VOEvent
 *
 * @param[in]  doc             The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns              XML Namespace
 * @param[in]  author_node     The node corresponding to the Author element
 * @param      voevent_author  A pointer to a VOEvent_Author structure
 */
void parse_author(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr author_node, struct VOEvent_Author *voevent_author) {
    xmlNodePtr cur;

    // set the elements to NULL in order to not have problems when Freeing the structure
    voevent_author->title        = NULL;
    voevent_author->shortName    = NULL;
    voevent_author->logoURL      = NULL;
    voevent_author->contactName  = NULL;
    voevent_author->contactEmail = NULL;
    voevent_author->contactPhone = NULL;
    voevent_author->contributor  = NULL;

    // REMEMBER: xmlNodeListGetString could return NULL

    cur = author_node->xmlChildrenNode;
    while (cur != NULL) {
        if (!xmlStrcmp(cur->name, (const xmlChar *) "title")){
            voevent_author->title = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "shortName")){
            voevent_author->shortName = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "logoURL")){
            voevent_author->logoURL = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "contactName")){
            voevent_author->contactName = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "contactEmail")){
            voevent_author->contactEmail = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "contactPhone")){
            voevent_author->contactPhone = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "contributor")){
            voevent_author->contributor = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        cur = cur->next;
    }

    return;
}

/**
 * @brief      Parse a Description element of a VOEvent
 *
 * @param[in]  doc                  The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                   XML Namespace
 * @param[in]  description_node     The node corresponding to the Description element
 * @param      voevent_description  A pointer to a VOEvent_Description structure
 */
void parse_description(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr description_node, struct VOEvent_Description *voevent_description){

    // set the elements to NULL in order to not have problems when Freeing the structure

    voevent_description->description = xmlNodeListGetString(doc, description_node->xmlChildrenNode, 1);
}

/**
 * @brief      Parse a Who element of a VOEvent
 *
 * @param[in]  doc          The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns           XML Namespace
 * @param[in]  who_node     The node corresponding to the Who element
 * @param      voevent_who  A pointer to a VOEvent_Who structure
 */
void parse_who(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr who_node, struct VOEvent_Who *voevent_who) {

    xmlNodePtr cur;

    // set the elements to NULL in order to not have problems when freeing the structure
    voevent_who->AuthorIVORN = NULL;
    voevent_who->Date        = NULL;
    voevent_who->Author      = NULL;
    voevent_who->Description = NULL;
    voevent_who->Reference   = NULL;

    /* We don't care what the top level element name is */
    cur = who_node->xmlChildrenNode;

    while (cur != NULL) {
        // REMEMBER: xmlNodeListGetString could return NULL

        if (!xmlStrcmp(cur->name, (const xmlChar *) "AuthorIVORN")){
            voevent_who->AuthorIVORN = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Date")){
            voevent_who->Date = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Author")){
            voevent_who->Author = (struct VOEvent_Author *) calloc(1, sizeof(struct VOEvent_Author));
            parse_author(doc, ns, cur, voevent_who->Author); // need to allocate Author first
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Description")){
            voevent_who->Description = (struct VOEvent_Description *) calloc(1, sizeof(struct VOEvent_Description));
            parse_description(doc, ns, cur, voevent_who->Description); // need to allocate Description first
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Reference")){
            voevent_who->Reference = (struct VOEvent_Reference *) calloc(1, sizeof(struct VOEvent_Reference));
            parse_reference(doc, ns, cur, voevent_who->Reference); // need to allocate Reference first
        }
        cur = cur->next;
    }

    return;
}

/**
 * @brief      { function_description }
 *
 * @param[in]  doc   The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns    XML Namespace
 * @param[in]  node  The node
 *
 * @return     { description_of_the_return_value }
 */
int find_nr_description(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr node){

    int nr_description = 0;
    xmlNodePtr cur = node->xmlChildrenNode;

    while (cur != NULL) {
        if (!xmlStrcmp(cur->name, (const xmlChar *) "Description")){
            nr_description++;
        }
        cur = cur->next;
    }

    return nr_description;

}

/**
 * @brief      { function_description }
 *
 * @param[in]  doc   The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns    XML Namespace
 * @param[in]  node  The node
 *
 * @return     { description_of_the_return_value }
 */
int find_nr_reference(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr node){

    int nr_reference = 0;
    xmlNodePtr cur = node->xmlChildrenNode;

    while (cur != NULL) {
        if (!xmlStrcmp(cur->name, (const xmlChar *) "Reference")){
            nr_reference++;
        }
        cur = cur->next;
    }

    return nr_reference;

}

/**
 * @brief      { function_description }
 *
 * @param[in]  doc   The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns    XML Namespace
 * @param[in]  node  The node
 *
 * @return     { description_of_the_return_value }
 */
int find_nr_params(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr node){

    int nr_params = 0;
    xmlNodePtr cur = node->xmlChildrenNode;

    while (cur != NULL) {
        if (!xmlStrcmp(cur->name, (const xmlChar *) "Param")){
            nr_params++;
        }
        cur = cur->next;
    }

    return nr_params;

}

/**
 * @brief      Parse a Value element of a VOEvent
 *
 * @param[in]  doc         The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns          XML Namespace
 * @param[in]  param_node  The node corresponding to the Parameter element
 * @param      param       A pointer to a VOEvent_Param structure
 */
void parse_param_value(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr param_node, struct VOEvent_Param *param){
    xmlNodePtr cur = param_node->xmlChildrenNode;

    while (cur != NULL) {
        if (!xmlStrcmp(cur->name, (const xmlChar *) "Value")){
            param->Value = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }
        cur = cur->next;
    }
}

/**
 * @brief      Parse a Param element of a VOEvent
 *
 * @param[in]  doc            The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns             XML Namespace
 * @param[in]  param_nodeset  The nodeset corresponding to the Param elements
 * @param      voevent_param  A pointer to a VOEvent_Param structure
 */
void parse_param(xmlDocPtr doc, xmlNsPtr ns, xmlNodeSetPtr param_nodeset, struct VOEvent_Param *voevent_param){

    int k;

    for (k = 0; k < param_nodeset->nodeNr; k++) {
        voevent_param[k].Description = NULL;
        voevent_param[k].Reference   = NULL;
        voevent_param[k].Value       = NULL;
        parse_param_value(doc, ns, param_nodeset->nodeTab[k], &(voevent_param[k]));
        voevent_param[k].name        = xmlGetProp(param_nodeset->nodeTab[k], (const xmlChar *) "name");
        voevent_param[k].ucd         = xmlGetProp(param_nodeset->nodeTab[k], (const xmlChar *) "ucd");
        voevent_param[k].value       = xmlGetProp(param_nodeset->nodeTab[k], (const xmlChar *) "value");
        voevent_param[k].unit        = xmlGetProp(param_nodeset->nodeTab[k], (const xmlChar *) "unit");
        voevent_param[k].dataType    = xmlGetProp(param_nodeset->nodeTab[k], (const xmlChar *) "dataType");
        voevent_param[k].utype       = xmlGetProp(param_nodeset->nodeTab[k], (const xmlChar *) "utype");

        if (voevent_param[k].name){ //needed if Param has no name, otherwise it will lead to invalid reads/writes

            char *xpath_param_description = calloc(1, (29+xmlStrlen(voevent_param[k].name)+1));
            sprintf(xpath_param_description, "//Param[@name='%s']/Description", voevent_param[k].name);

            xmlXPathObjectPtr param_description = getnodeset(doc, (xmlChar *)xpath_param_description);

            if(param_description){
                int nr_description = 0;
                int j;
                xmlNodeSetPtr nodeset_param_description = param_description->nodesetval;
                nr_description = nodeset_param_description->nodeNr;
                voevent_param[k].nr_description = nr_description;
                voevent_param[k].Description = calloc(nr_description, sizeof(struct VOEvent_Description));

                for(j = 0; j < nr_description; j++){
                    parse_description(doc, ns, nodeset_param_description->nodeTab[j], &(voevent_param[k].Description[j]));
                }

                xmlXPathFreeObject(param_description);
            }

            free(xpath_param_description);

            char *xpath_param_reference = calloc(1, (27+xmlStrlen(voevent_param[k].name)+1));
            sprintf(xpath_param_reference, "//Param[@name='%s']/Reference", voevent_param[k].name);

            xmlXPathObjectPtr param_reference = getnodeset(doc, (xmlChar *)xpath_param_reference);

            if(param_reference){
                int nr_reference = 0;
                int j;
                xmlNodeSetPtr nodeset_param_reference = param_reference->nodesetval;
                nr_reference = nodeset_param_reference->nodeNr;
                voevent_param[k].nr_reference = nr_reference;
                voevent_param[k].Reference = calloc(nr_reference, sizeof(struct VOEvent_Reference));

                for(j = 0; j < nr_reference; j++){
                    parse_reference(doc, ns, nodeset_param_reference->nodeTab[j], &(voevent_param[k].Reference[j]));
                }

                xmlXPathFreeObject(param_reference);
            }

        free(xpath_param_reference);

        }
    }
}

/**
 * @brief      Parse a Group element of a VOEvent
 *
 * The different sub-elements of the Group elements (Param, Description, Reference)
 * are retrieved using an XPath expression. Either the name or the type of the Group
 * is used in the XPath expression, because sometimes a Group does not have a name.
 *
 * @param[in]  doc            The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns             XML Namespace
 * @param[in]  group_nodeset  The nodeset corresponding to the Group elements
 * @param      voevent_group  A pointer to a VOEvent_Group structure
 */
void parse_group(xmlDocPtr doc, xmlNsPtr ns, xmlNodeSetPtr group_nodeset, struct VOEvent_Group *voevent_group){

    int k;

    for (k = 0; k < group_nodeset->nodeNr; k++) {
        voevent_group[k].Param       = NULL;
        voevent_group[k].Description = NULL;
        voevent_group[k].Reference   = NULL;
        voevent_group[k].name        = xmlGetProp(group_nodeset->nodeTab[k], (const xmlChar *) "name");
        voevent_group[k].type        = xmlGetProp(group_nodeset->nodeTab[k], (const xmlChar *) "type");

        //xmlNodePtr cur = group_nodeset->nodeTab[k];

        //voevent_group[k].nr_params      = find_nr_params(doc, ns, cur);
        //voevent_group[k].nr_description = find_nr_description(doc, ns, cur);
        //voevent_group[k].nr_reference   = find_nr_reference(doc, ns, cur);

        if (voevent_group[k].name){

            char *xpath_group_param = calloc(1, (28+xmlStrlen(voevent_group[k].name)+1));
            sprintf(xpath_group_param, "//What/Group[@name='%s']/Param", voevent_group[k].name);

            xmlXPathObjectPtr group_param = getnodeset(doc, (xmlChar *)xpath_group_param);

            if(group_param){
                int nr_params = 0;
                xmlNodeSetPtr nodeset_group_param = group_param->nodesetval;
                nr_params     = nodeset_group_param->nodeNr;
                voevent_group[k].nr_params = nr_params;
                voevent_group[k].Param = calloc(nr_params, sizeof(struct VOEvent_Param));

                parse_param(doc, ns, nodeset_group_param, voevent_group[k].Param);
                xmlXPathFreeObject(group_param);
            }

            free(xpath_group_param);

            char *xpath_group_description = calloc(1, (34+xmlStrlen(voevent_group[k].name)+1));
            sprintf(xpath_group_description, "//What/Group[@name='%s']/Description", voevent_group[k].name);

            xmlXPathObjectPtr group_description = getnodeset(doc, (xmlChar *)xpath_group_description);

            if(group_description){
                int nr_description = 0;
                int j;
                xmlNodeSetPtr nodeset_group_description = group_description->nodesetval;
                nr_description = nodeset_group_description->nodeNr;
                voevent_group[k].nr_description = nr_description;
                voevent_group[k].Description = calloc(nr_description, sizeof(struct VOEvent_Description));

                for(j = 0; j < nr_description; j++){
                    parse_description(doc, ns, nodeset_group_description->nodeTab[j], &(voevent_group[k].Description[j]));
                }

                xmlXPathFreeObject(group_description);
            }

            free(xpath_group_description);

            char *xpath_group_reference = calloc(1, (32+xmlStrlen(voevent_group[k].name)+1));
            sprintf(xpath_group_reference, "//What/Group[@name='%s']/Reference", voevent_group[k].name);

            xmlXPathObjectPtr group_reference = getnodeset(doc, (xmlChar *)xpath_group_reference);

            if(group_reference){
                int nr_reference = 0;
                int w;
                xmlNodeSetPtr nodeset_group_reference = group_reference->nodesetval;
                nr_reference = nodeset_group_reference->nodeNr;
                voevent_group[k].nr_reference = nr_reference;
                voevent_group[k].Reference = calloc(nr_reference, sizeof(struct VOEvent_Reference));

                for(w = 0; w < nr_reference; w++){
                    parse_reference(doc, ns, nodeset_group_reference->nodeTab[w], &(voevent_group[k].Reference[w]));
                }

                xmlXPathFreeObject(group_reference);
            }

            free(xpath_group_reference);

        }
        else if(voevent_group[k].type){

            char *xpath_group_param = calloc(1, (28+xmlStrlen(voevent_group[k].type)+1));
            sprintf(xpath_group_param, "//What/Group[@type='%s']/Param", voevent_group[k].type);

            xmlXPathObjectPtr group_param = getnodeset(doc, (xmlChar *)xpath_group_param);

            if(group_param){
                int nr_params = 0;
                xmlNodeSetPtr nodeset_group_param = group_param->nodesetval;
                nr_params     = nodeset_group_param->nodeNr;
                voevent_group[k].nr_params = nr_params;
                voevent_group[k].Param = calloc(nr_params, sizeof(struct VOEvent_Param));

                parse_param(doc, ns, nodeset_group_param, voevent_group[k].Param);
                xmlXPathFreeObject(group_param);
            }

            free(xpath_group_param);

            char *xpath_group_description = calloc(1, (34+xmlStrlen(voevent_group[k].type)+1));
            sprintf(xpath_group_description, "//What/Group[@type='%s']/Description", voevent_group[k].type);

            xmlXPathObjectPtr group_description = getnodeset(doc, (xmlChar *)xpath_group_description);

            if(group_description){
                int nr_description = 0;
                int j;
                xmlNodeSetPtr nodeset_group_description = group_description->nodesetval;
                nr_description = nodeset_group_description->nodeNr;
                voevent_group[k].nr_description = nr_description;
                voevent_group[k].Description = calloc(nr_description, sizeof(struct VOEvent_Description));

                for(j = 0; j < nr_description; j++){
                    parse_description(doc, ns, nodeset_group_description->nodeTab[j], &(voevent_group[k].Description[j]));
                }

                xmlXPathFreeObject(group_description);
            }

            free(xpath_group_description);

            char *xpath_group_reference = calloc(1, (32+xmlStrlen(voevent_group[k].type)+1));
            sprintf(xpath_group_reference, "//What/Group[@type='%s']/Reference", voevent_group[k].type);

            xmlXPathObjectPtr group_reference = getnodeset(doc, (xmlChar *)xpath_group_reference);

            if(group_reference){
                int nr_reference = 0;
                int w;
                xmlNodeSetPtr nodeset_group_reference = group_reference->nodesetval;
                nr_reference = nodeset_group_reference->nodeNr;
                voevent_group[k].nr_reference = nr_reference;
                voevent_group[k].Reference = calloc(nr_reference, sizeof(struct VOEvent_Reference));

                for(w = 0; w < nr_reference; w++){
                    parse_reference(doc, ns, nodeset_group_reference->nodeTab[w], &(voevent_group[k].Reference[w]));
                }

                xmlXPathFreeObject(group_reference);
            }

            free(xpath_group_reference);

        }
    }
}

/**
 * @brief      Parse a Field element of a VOEvent
 *
 * @param[in]  doc            The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns             XML Namespace
 * @param[in]  field_nodeset  The nodeset corresponding to the Field elements
 * @param      voevent_field  A pointer to a VOEvent_Field structure
 */
void parse_field(xmlDocPtr doc, xmlNsPtr ns, xmlNodeSetPtr field_nodeset, struct VOEvent_Field *voevent_field){

    int k;

    for (k = 0; k < field_nodeset->nodeNr; k++) {
        voevent_field[k].Description = NULL;
        voevent_field[k].Reference   = NULL;
        voevent_field[k].name        = xmlGetProp(field_nodeset->nodeTab[k], (const xmlChar *) "name");
        voevent_field[k].ucd         = xmlGetProp(field_nodeset->nodeTab[k], (const xmlChar *) "ucd");
        voevent_field[k].unit        = xmlGetProp(field_nodeset->nodeTab[k], (const xmlChar *) "unit");
        voevent_field[k].dataType    = xmlGetProp(field_nodeset->nodeTab[k], (const xmlChar *) "dataType");
        voevent_field[k].utype       = xmlGetProp(field_nodeset->nodeTab[k], (const xmlChar *) "utype");

        if(voevent_field[k].name){

            char *xpath_field_description = calloc(1, (29+xmlStrlen(voevent_field[k].name)+1));
            sprintf(xpath_field_description, "//Field[@name='%s']/Description", voevent_field[k].name);

            xmlXPathObjectPtr field_description = getnodeset(doc, (xmlChar *)xpath_field_description);

            if(field_description){
                int nr_description = 0;
                int j;
                xmlNodeSetPtr nodeset_field_description = field_description->nodesetval;
                nr_description = nodeset_field_description->nodeNr;
                voevent_field[k].nr_description = nr_description;
                voevent_field[k].Description = calloc(nr_description, sizeof(struct VOEvent_Description));

                for(j = 0; j < nr_description; j++){
                    parse_description(doc, ns, nodeset_field_description->nodeTab[j], &(voevent_field[k].Description[j]));
                }

                xmlXPathFreeObject(field_description);
            }

            free(xpath_field_description);

            char *xpath_field_reference = calloc(1, (27+xmlStrlen(voevent_field[k].name)+1));
            sprintf(xpath_field_reference, "//Field[@name='%s']/Reference", voevent_field[k].name);

            xmlXPathObjectPtr field_reference = getnodeset(doc, (xmlChar *)xpath_field_reference);

            if(field_reference){
                int nr_reference = 0;
                int j;
                xmlNodeSetPtr nodeset_field_reference = field_reference->nodesetval;
                nr_reference = nodeset_field_reference->nodeNr;
                voevent_field[k].nr_reference = nr_reference;
                voevent_field[k].Reference = calloc(nr_reference, sizeof(struct VOEvent_Reference));

                for(j = 0; j < nr_reference; j++){
                    parse_reference(doc, ns, nodeset_field_reference->nodeTab[j], &(voevent_field[k].Reference[j]));
                }

                xmlXPathFreeObject(field_reference);
            }

            free(xpath_field_reference);
        }
    }
}

/**
 * @brief      Parse a Data element of a VOEvent
 *
 * @param[in]  doc           The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns            XML Namespace
 * @param[in]  data_nodeset  The nodeset corresponding to the Data elements
 * @param      voevent_data  A pointer to a VOEvent_Data structure
 */
void parse_data(xmlDocPtr doc, xmlNsPtr ns, xmlNodeSetPtr data_nodeset, struct VOEvent_Data *voevent_data){

    int k;

    for (k = 0; k < data_nodeset->nodeNr; k++) {
        voevent_data[k].data = xmlNodeListGetString(doc, data_nodeset->nodeTab[k]->xmlChildrenNode, 1);
    }

    printf("Done parsing data.\n");

}

/**
 * @brief      Parse a Table element of a VOEvent
 *
 * The set of Table elements is retrieved using an XPath expression.
 * Instead of the table name, a general approach uses the table number
 * in the XPath expression, since the number of Table elements is known.
 * Using the Table names would require searching for them, create a list
 * of names and loop over it, which is less elegant and more difficult than
 * using a simple interger number.
 *
 * @param[in]  doc            The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns             XML Namespace
 * @param[in]  table_nodeset  The nodeset corresponding to the Table elements
 * @param      voevent_table  A pointer to a VOEvent_Table structure
 */
void parse_table(xmlDocPtr doc, xmlNsPtr ns, xmlNodeSetPtr table_nodeset, struct VOEvent_Table *voevent_table){
    int k;

    for (k = 0; k < table_nodeset->nodeNr; k++) {
        voevent_table[k].Param       = NULL;
        voevent_table[k].Description = NULL;
        voevent_table[k].Reference   = NULL;
        voevent_table[k].Field       = NULL;
        voevent_table[k].Data        = NULL;

        voevent_table[k].name        = xmlGetProp(table_nodeset->nodeTab[k], (const xmlChar *) "name");
        voevent_table[k].type        = xmlGetProp(table_nodeset->nodeTab[k], (const xmlChar *) "type");

        //xmlNodePtr cur = table_nodeset->nodeTab[k];

        //voevent_table[k].nr_params      = find_nr_params(doc, ns, cur);
        //voevent_table[k].nr_description = find_nr_description(doc, ns, cur);
        //voevent_table[k].nr_reference   = find_nr_reference(doc, ns, cur);

        // DO NOT USE TABLE NAME, BUT ONLY TABLE NUMBER IN XPATH EXPRESSION
        // IT IS A MORE GENERAL APPROACH

        int no_table = snprintf(NULL, 0, "%d", k+1);
        char *xpath_table_param = calloc(1, (20+no_table+1));
        sprintf(xpath_table_param, "//What/Table[%d]/Param", k+1);

        xmlXPathObjectPtr table_param = getnodeset(doc, (xmlChar *)xpath_table_param);

        if(table_param){
            int nr_params = 0;
            xmlNodeSetPtr nodeset_table_param = table_param->nodesetval;
            nr_params     = nodeset_table_param->nodeNr;
            voevent_table[k].nr_params = nr_params;
            voevent_table[k].Param = calloc(nr_params, sizeof(struct VOEvent_Param));

            parse_param(doc, ns, nodeset_table_param, voevent_table[k].Param);
            xmlXPathFreeObject(table_param);
        }

        free(xpath_table_param);

        char *xpath_table_description = calloc(1, (26+no_table+1));
        sprintf(xpath_table_description, "//What/Table[%d]/Description", k+1);

        xmlXPathObjectPtr table_description = getnodeset(doc, (xmlChar *)xpath_table_description);

        if(table_description){
            int nr_description = 0;
            int j;
            xmlNodeSetPtr nodeset_table_description = table_description->nodesetval;
            nr_description = nodeset_table_description->nodeNr;
            voevent_table[k].nr_description = nr_description;
            voevent_table[k].Description = calloc(nr_description, sizeof(struct VOEvent_Description));

            for(j = 0; j < nr_description; j++){
                parse_description(doc, ns, nodeset_table_description->nodeTab[j], &(voevent_table[k].Description[j]));
            }

            xmlXPathFreeObject(table_description);
        }

        free(xpath_table_description);

        char *xpath_table_reference = calloc(1, (24+no_table+1));
        sprintf(xpath_table_reference, "//What/Table[%d]/Reference", k+1);

        xmlXPathObjectPtr table_reference = getnodeset(doc, (xmlChar *)xpath_table_reference);

        if(table_reference){
            int nr_reference = 0;
            int w;
            xmlNodeSetPtr nodeset_table_reference = table_reference->nodesetval;
            nr_reference = nodeset_table_reference->nodeNr;
            voevent_table[k].nr_reference = nr_reference;
            voevent_table[k].Reference = calloc(nr_reference, sizeof(struct VOEvent_Reference));

            for(w = 0; w < nr_reference; w++){
                parse_reference(doc, ns, nodeset_table_reference->nodeTab[w], &(voevent_table[k].Reference[w]));
            }

            xmlXPathFreeObject(table_reference);
        }

        free(xpath_table_reference);

        char *xpath_table_field = calloc(1, (20+no_table+1));
        sprintf(xpath_table_field, "//What/Table[%d]/Field", k+1);

        xmlXPathObjectPtr table_field = getnodeset(doc, (xmlChar *)xpath_table_field);

        if(table_field){
            int nr_fields = 0;
            xmlNodeSetPtr nodeset_table_field = table_field->nodesetval;
            nr_fields = nodeset_table_field->nodeNr;
            voevent_table[k].nr_columns = nr_fields;
            voevent_table[k].Field = calloc(nr_fields, sizeof(struct VOEvent_Field));

            parse_field(doc, ns, nodeset_table_field, voevent_table[k].Field);
            xmlXPathFreeObject(table_field);
        }

        free(xpath_table_field);

        char *xpath_table_td = calloc(1, (25+no_table+1));
        sprintf(xpath_table_td, "//What/Table[%d]/Data/TR/TD", k+1);

        xmlXPathObjectPtr table_td = getnodeset(doc, (xmlChar *)xpath_table_td);

        if(table_td){
            int nr_td = 0;
            xmlNodeSetPtr nodeset_table_td = table_td->nodesetval;
            nr_td = nodeset_table_td->nodeNr;
            voevent_table[k].nr_rows = nr_td/voevent_table[k].nr_columns;
            voevent_table[k].Data = calloc(nr_td, sizeof(struct VOEvent_Data));

            parse_data(doc, ns, nodeset_table_td, voevent_table[k].Data);
            xmlXPathFreeObject(table_td);
        }

        free(xpath_table_td);

    }

}

/**
 * @brief      Parse a What element of a VOEvent
 *
 * To get all the Param, Group and Table elements of the What element,
 * XPath expressions are used.
 *
 * @param[in]  doc           The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns            XML Namespace
 * @param[in]  what_node     The node corresponding to the How element
 * @param      voevent_what  A pointer to a VOEvent_What structure
 */
void parse_what(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr what_node, struct VOEvent_What *voevent_what){
    // set the elements to NULL in order to not have problems when freeing the structure

    voevent_what->Param       = NULL;
    voevent_what->Group       = NULL;
    voevent_what->Description = NULL;
    voevent_what->Reference   = NULL;
    voevent_what->Table       = NULL;

    xmlXPathObjectPtr param = getnodeset(doc, (xmlChar *)"//What/Param");
    xmlXPathObjectPtr group = getnodeset(doc, (xmlChar *)"//What/Group");
    xmlXPathObjectPtr table = getnodeset(doc, (xmlChar *)"//What/Table");

    // add description and reference, again with nodeset e.g. //What/Description and //What/Reference

    if(param){
        int nr_params = 0;
        xmlNodeSetPtr nodeset_param = param->nodesetval;
        nr_params     = nodeset_param->nodeNr;
        voevent_what->nr_params = nr_params;
        voevent_what->Param = calloc(nr_params, sizeof(struct VOEvent_Param));

        parse_param(doc, ns, nodeset_param, voevent_what->Param);
        xmlXPathFreeObject(param);
    }

    if(group){
        int nr_groups = 0;
        xmlNodeSetPtr nodeset_group = group->nodesetval;
        nr_groups     = nodeset_group->nodeNr;
        voevent_what->nr_groups = nr_groups;
        voevent_what->Group = calloc(nr_groups, sizeof(struct VOEvent_Group));

        parse_group(doc, ns, nodeset_group, voevent_what->Group);
        xmlXPathFreeObject(group);
    }

    if(table){
        int nr_tables = 0;
        xmlNodeSetPtr nodeset_table = table->nodesetval;
        nr_tables     = nodeset_table->nodeNr;
        voevent_what->nr_tables = nr_tables;
        voevent_what->Table = calloc(nr_tables, sizeof(struct VOEvent_Table));

        parse_table(doc, ns, nodeset_table, voevent_what->Table);
        xmlXPathFreeObject(table);
    }

    return;
}

/**
 * @brief      Parse a How element of a VOEvent
 *
 * @param[in]  doc          The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns           XML Namespace
 * @param[in]  how_node     The node corresponding to the How element
 * @param      voevent_how  A pointer to a VOEvent_How structure
 */
void parse_how(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr how_node, struct VOEvent_How *voevent_how){

    // set the elements to NULL in order to not have problems when freeing the structure
    voevent_how->Reference   = NULL;
    voevent_how->Description = NULL;

    xmlXPathObjectPtr description;
    xmlXPathObjectPtr reference;

    reference   = getnodeset(doc, (xmlChar *)"//How/Reference");
    description = getnodeset(doc, (xmlChar *)"//How/Description");

    xmlNodeSetPtr nodeset_reference;
    xmlNodeSetPtr nodeset_description;

    if(reference){
        int nr_reference = 0;
        int j = 0;
        nodeset_reference = reference->nodesetval;
        nr_reference      = nodeset_reference->nodeNr;
        voevent_how->nr_reference = nr_reference;
        voevent_how->Reference = calloc(nr_reference, sizeof(struct VOEvent_Reference));
        for (j = 0; j < nr_reference; j++) {
            parse_reference(doc, ns, nodeset_reference->nodeTab[j], &(voevent_how->Reference[j]));
        }
        xmlXPathFreeObject(reference);
    }

    if(description){
        int nr_description = 0;
        int k = 0;
        nodeset_description = description->nodesetval;
        nr_description      = nodeset_description->nodeNr;
        voevent_how->nr_description = nr_description;
        voevent_how->Description = calloc(nr_description, sizeof(struct VOEvent_Description));
        for (k = 0; k < nr_description; k++) {
            parse_description(doc, ns, nodeset_description->nodeTab[k], &(voevent_how->Description[k]));
        }
        xmlXPathFreeObject(description);
    }

    return;
}

/**
 * @brief      Parse a TimeInstant element of a VOEvent
 *
 * @param[in]  doc                  The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                   XML Namespace
 * @param[in]  timeinstant_node     The node corresponding to the TimeInstant element
 * @param      voevent_timeinstant  A pointer to a VOEvent_TimeInstant structure
 */
void parse_timeinstant(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr timeinstant_node, struct VOEvent_TimeInstant *voevent_timeinstant){

    xmlNodePtr cur;

    voevent_timeinstant->ISOTime     = NULL;
    voevent_timeinstant->TimeOffset  = NULL;
    voevent_timeinstant->TimeScale   = NULL;

    cur = timeinstant_node->xmlChildrenNode;

    while(cur != NULL){

        if (!xmlStrcmp(cur->name, (const xmlChar *) "ISOTime")){
            voevent_timeinstant->ISOTime = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "TimeOffset")){
            voevent_timeinstant->TimeOffset = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "TimeScale")){
            voevent_timeinstant->TimeScale = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        cur = cur->next;
    }

    return;
}

/**
 * @brief      Parse a Time element of a VOEvent
 *
 * @param[in]  doc           The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns            XML Namespace
 * @param[in]  time_node     The node corresponding to the Time element
 * @param      voevent_time  A pointer to a VOEvent_Time structure
 */
void parse_time(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr time_node, struct VOEvent_Time *voevent_time){

    xmlNodePtr cur;

    voevent_time->TimeInstant = NULL;
    voevent_time->Error       = NULL;

    cur = time_node->xmlChildrenNode;

    while(cur != NULL){

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Error")){
            voevent_time->Error = xmlGetProp(cur, (const xmlChar *) "Error");
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "TimeInstant")){
            voevent_time->TimeInstant = (struct VOEvent_TimeInstant *) calloc(1, sizeof(struct VOEvent_TimeInstant));
            parse_timeinstant(doc, ns, cur, voevent_time->TimeInstant);
        }

        cur = cur->next;
    }

    return;
}

/**
 * @brief      Parse a Position2D element of a VOEvent
 *
 * @param[in]  doc                 The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                  XML Namespace
 * @param[in]  position2d_node     The node corresponding to the Position2D element
 * @param      voevent_position2d  A pointer to a VOEvent_Position2D structure
 */
void parse_position2d(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr position2d_node, struct VOEvent_Position2D *voevent_position2d){

    xmlNodePtr cur;

    voevent_position2d->Name1       = NULL;
    voevent_position2d->Name2       = NULL;
    voevent_position2d->C1          = NULL;
    voevent_position2d->C2          = NULL;
    voevent_position2d->ErrorRadius = NULL;

    voevent_position2d->unit = xmlGetProp(position2d_node, (const xmlChar *) "unit");

    cur = position2d_node->xmlChildrenNode;

    while(cur != NULL){

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Name1")){
            voevent_position2d->Name1 = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Name2")){
            voevent_position2d->Name2 = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Value2")){
            xmlNodePtr value = cur->xmlChildrenNode;

            while(value != NULL){

                if (!xmlStrcmp(value->name, (const xmlChar *) "C1")){
                    voevent_position2d->C1 = xmlNodeListGetString(doc, value->xmlChildrenNode, 1);
                }

                if (!xmlStrcmp(value->name, (const xmlChar *) "C2")){
                    voevent_position2d->C2 = xmlNodeListGetString(doc, value->xmlChildrenNode, 1);
                }

                value = value->next;
            }

        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Error2Radius")){
            voevent_position2d->ErrorRadius = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        cur = cur->next;
    }

    return;

}

/**
 * @brief      Parse a Position3D element of a VOEvent
 *
 * @param[in]  doc                 The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                  XML Namespace
 * @param[in]  position3d_node     The node corresponding to the Position3D element
 * @param      voevent_position3d  A pointer to a VOEvent_Position3D structure
 */
void parse_position3d(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr position3d_node, struct VOEvent_Position3D *voevent_position3d){

    xmlNodePtr cur;

    voevent_position3d->Name1 = NULL;
    voevent_position3d->Name2 = NULL;
    voevent_position3d->Name3 = NULL;
    voevent_position3d->C1    = NULL;
    voevent_position3d->C2    = NULL;
    voevent_position3d->C3    = NULL;

    voevent_position3d->unit = xmlGetProp(position3d_node, (const xmlChar *) "unit");

    cur = position3d_node->xmlChildrenNode;

    while(cur != NULL){

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Name1")){
            voevent_position3d->Name1 = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Name2")){
            voevent_position3d->Name2 = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Name3")){
            voevent_position3d->Name3 = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Value3")){
            xmlNodePtr value = cur->xmlChildrenNode;

            while(value != NULL){

                if (!xmlStrcmp(value->name, (const xmlChar *) "C1")){
                    voevent_position3d->C1 = xmlNodeListGetString(doc, value->xmlChildrenNode, 1);
                }

                if (!xmlStrcmp(value->name, (const xmlChar *) "C2")){
                    voevent_position3d->C2 = xmlNodeListGetString(doc, value->xmlChildrenNode, 1);
                }

                if (!xmlStrcmp(value->name, (const xmlChar *) "C3")){
                    voevent_position3d->C3 = xmlNodeListGetString(doc, value->xmlChildrenNode, 1);
                }

                value = value->next;
            }

        }

        cur = cur->next;
    }

    return;

}

/**
 * @brief      Parse a AstroCoords element of a VOEvent
 *
 * @param[in]  doc                  The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                   XML Namespace
 * @param[in]  astrocoords_node     The node corresponding to the AstroCoords element
 * @param      voevent_astrocoords  A pointer to a VOEvent_AstroCoords structure
 */
void parse_astrocoords(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr astrocoords_node, struct VOEvent_AstroCoords *voevent_astrocoords){

    xmlNodePtr cur;

    voevent_astrocoords->Time       = NULL;
    voevent_astrocoords->Position2D = NULL;
    voevent_astrocoords->Position3D = NULL;

    voevent_astrocoords->coord_system_id = xmlGetProp(astrocoords_node, (const xmlChar *) "coord_system_id");

    cur = astrocoords_node->xmlChildrenNode;

    while(cur != NULL){

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Time")){
            voevent_astrocoords->Time = (struct VOEvent_Time *) calloc(1, sizeof(struct VOEvent_Time));
            parse_time(doc, ns, cur, voevent_astrocoords->Time);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Position2D")){
            voevent_astrocoords->Position2D = (struct VOEvent_Position2D *) calloc(1, sizeof(struct VOEvent_Position2D));
            parse_position2d(doc, ns, cur, voevent_astrocoords->Position2D);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Position3D")){
            voevent_astrocoords->Position3D = (struct VOEvent_Position3D *) calloc(1, sizeof(struct VOEvent_Position3D));
            parse_position3d(doc, ns, cur, voevent_astrocoords->Position3D);
        }

        cur = cur->next;
    }

    return;
}

/**
 * @brief      Parse a ObservatoryLocation element of a VOEvent
 *
 * @param[in]  doc                          The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                           XML Namespace
 * @param[in]  observatorylocation_node     The node corresponding to the ObservatoryLocation element
 * @param      voevent_observatorylocation  A pointer to a VOEvent_ObservatoryLocation structure
 */
void parse_observatorylocation(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr observatorylocation_node, struct VOEvent_ObservatoryLocation *voevent_observatorylocation){

    xmlNodePtr cur;

    // set the elements to NULL in order to not have problems when freeing the structure
    voevent_observatorylocation->AstroCoordSystem = NULL;
    voevent_observatorylocation->AstroCoords      = NULL;

    voevent_observatorylocation->id = xmlGetProp(observatorylocation_node, (const xmlChar *) "id");

    /* We don't care what the top level element name is */
    cur = observatorylocation_node->xmlChildrenNode;

    while (cur != NULL) {
        // REMEMBER: xmlNodeListGetString could return NULL

        if (!xmlStrcmp(cur->name, (const xmlChar *) "AstroCoordSystem")){
            voevent_observatorylocation->AstroCoordSystem = xmlGetProp(cur, (const xmlChar *) "id");
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "AstroCoords")){
            voevent_observatorylocation->AstroCoords = (struct VOEvent_AstroCoords *) calloc(1, sizeof(struct VOEvent_AstroCoords));
            parse_astrocoords(doc, ns, cur, voevent_observatorylocation->AstroCoords); // need to allocate ObsDataLocation first
        }

        cur = cur->next;
    }

    return;
}

/**
 * @brief      Parse a ObservationLocation element of a VOEvent
 *
 * @param[in]  doc                          The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                           XML Namespace
 * @param[in]  observationlocation_node     The node corresponding to the ObservationLocation element
 * @param      voevent_observationlocation  A pointer to a VOEvent_ObservationLocation structure
 */
void parse_observationlocation(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr observationlocation_node, struct VOEvent_ObservationLocation *voevent_observationlocation){

    xmlNodePtr cur;

    // set the elements to NULL in order to not have problems when freeing the structure
    voevent_observationlocation->AstroCoordSystem = NULL;
    voevent_observationlocation->AstroCoords      = NULL;

    /* We don't care what the top level element name is */
    cur = observationlocation_node->xmlChildrenNode;

    while (cur != NULL) {
        // REMEMBER: xmlNodeListGetString could return NULL

        if (!xmlStrcmp(cur->name, (const xmlChar *) "AstroCoordSystem")){
            voevent_observationlocation->AstroCoordSystem = xmlGetProp(cur, (const xmlChar *) "id");
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "AstroCoords")){
            voevent_observationlocation->AstroCoords = (struct VOEvent_AstroCoords *) calloc(1, sizeof(struct VOEvent_AstroCoords));
            parse_astrocoords(doc, ns, cur, voevent_observationlocation->AstroCoords); // need to allocate ObsDataLocation first
        }

        cur = cur->next;
    }

    return;
}

/**
 * @brief      Parse a ObsDataLocation element of a VOEvent
 *
 * @param[in]  doc                      The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                       XML Namespace
 * @param[in]  obsdatalocation_node     The node corresponding to the ObsDataLocation element
 * @param      voevent_obsdatalocation  A pointer to a VOEvent_ObsDataLocation structure
 */
void parse_obsdatalocation(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr obsdatalocation_node, struct VOEvent_ObsDataLocation *voevent_obsdatalocation){

    xmlNodePtr cur;

    // set the elements to NULL in order to not have problems when freeing the structure
    voevent_obsdatalocation->ObservatoryLocation = NULL;
    voevent_obsdatalocation->ObservationLocation = NULL;

    /* We don't care what the top level element name is */
    cur = obsdatalocation_node->xmlChildrenNode;

    while (cur != NULL) {
        // REMEMBER: xmlNodeListGetString could return NULL

        if (!xmlStrcmp(cur->name, (const xmlChar *) "ObservatoryLocation")){
            voevent_obsdatalocation->ObservatoryLocation = (struct VOEvent_ObservatoryLocation *) calloc(1, sizeof(struct VOEvent_ObservatoryLocation));
            parse_observatorylocation(doc, ns, cur, voevent_obsdatalocation->ObservatoryLocation); // need to allocate ObsDataLocation first
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "ObservationLocation")){
            voevent_obsdatalocation->ObservationLocation = (struct VOEvent_ObservationLocation *) calloc(1, sizeof(struct VOEvent_ObservationLocation));
            parse_observationlocation(doc, ns, cur, voevent_obsdatalocation->ObservationLocation); // need to allocate ObsDataLocation first
        }

        cur = cur->next;
    }

    return;
}

/**
 * @brief      Parse a WhereWhen element of a VOEvent
 *
 * @param[in]  doc                The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                 XML Namespace
 * @param[in]  wherewhen_node     The node corresponding to the WhereWhen element
 * @param      voevent_wherewhen  A pointer to a VOEvent_WhereWhen structure
 */
void parse_wherewhen(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr wherewhen_node, struct VOEvent_WhereWhen *voevent_wherewhen){

    xmlNodePtr cur;

    // set the elements to NULL in order to not have problems when freeing the structure
    voevent_wherewhen->ObsDataLocation = NULL;
    voevent_wherewhen->Description     = NULL;
    voevent_wherewhen->Reference       = NULL;

    /* We don't care what the top level element name is */
    cur = wherewhen_node->xmlChildrenNode;

    while (cur != NULL) {
        // REMEMBER: xmlNodeListGetString could return NULL

        if (!xmlStrcmp(cur->name, (const xmlChar *) "ObsDataLocation")){
            voevent_wherewhen->ObsDataLocation = (struct VOEvent_ObsDataLocation *) calloc(1, sizeof(struct VOEvent_ObsDataLocation));
            parse_obsdatalocation(doc, ns, cur, voevent_wherewhen->ObsDataLocation); // need to allocate ObsDataLocation first
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Description")){
            voevent_wherewhen->Description = (struct VOEvent_Description *) calloc(1, sizeof(struct VOEvent_Description));
            parse_description(doc, ns, cur, voevent_wherewhen->Description); // need to allocate Description first
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Reference")){
            voevent_wherewhen->Reference = (struct VOEvent_Reference *) calloc(1, sizeof(struct VOEvent_Reference));
            parse_reference(doc, ns, cur, voevent_wherewhen->Reference); // need to allocate Reference first
        }
        cur = cur->next;
    }

    return;
}

/**
 * @brief      Parse a EventIVORN element of a VOEvent
 *
 * All the EventIVORN elements are retrieved using an XPath expression
 *
 * @param[in]  doc                The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                 XML Namespace
 * @param      voevent_citations  A pointer to a VOEvent_Citations structure
 */
void parse_event_ivorn(xmlDocPtr doc, xmlNsPtr ns, struct VOEvent_Citations *voevent_citations){

    xmlXPathObjectPtr event_ivorn;
    event_ivorn = getnodeset(doc, (xmlChar *)"//Citations/EventIVORN");
    xmlNodeSetPtr nodeset_event_ivorn;

    if(event_ivorn){
        int nr_event_ivorn = 0;
        int j = 0;
        nodeset_event_ivorn = event_ivorn->nodesetval;
        nr_event_ivorn      = nodeset_event_ivorn->nodeNr;
        voevent_citations->nr_event_ivorn = nr_event_ivorn;
        voevent_citations->EventIVORN = calloc(nr_event_ivorn, sizeof(struct VOEvent_EventIVORN));
        for (j = 0; j < nr_event_ivorn; j++) {
          voevent_citations->EventIVORN[j].cite  = xmlGetProp(nodeset_event_ivorn->nodeTab[j], (const xmlChar *) "cite");
          voevent_citations->EventIVORN[j].ivorn = xmlNodeListGetString(doc, nodeset_event_ivorn->nodeTab[j]->xmlChildrenNode, 1);
        }
        xmlXPathFreeObject(event_ivorn);
    }

    return;
}

/**
 * @brief      Parse a Citation element of a VOEvent
 *
 * @param[in]  doc                The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                 XML Namespace
 * @param[in]  cit_node           The node corresponding to the Citations element
 * @param      voevent_citations  A pointer to a VOEvent_Citations structure
 */
void parse_citations(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cit_node, struct VOEvent_Citations *voevent_citations) {
    xmlNodePtr cur;

    // set the elements to NULL in order to not have problems when freeing the structure
    voevent_citations->EventIVORN  = NULL;
    voevent_citations->Description = NULL;
    voevent_citations->nr_event_ivorn = 0;

    cur = cit_node->xmlChildrenNode;
    // only one Description is there according to the VOEvent schema. No need to use xpath, but simple nodes parsing is enough
    while (cur != NULL) {

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Description")){
            voevent_citations->Description = (struct VOEvent_Description *) calloc(1, sizeof(struct VOEvent_Description));
            parse_description(doc, ns, cur, voevent_citations->Description);
        }

        cur = cur->next;
    }

    parse_event_ivorn(doc, ns, voevent_citations);

    return;
}

/**
 * @brief      Parse a Inference element of a VOEvent
 *
 * @param[in]  doc                The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns                 XML Namespace
 * @param[in]  inference_nodeset  The node corresponding to the Inference element
 * @param      voevent_inference  A pointer to a VOEvent_Inference structure
 */
void parse_inference(xmlDocPtr doc, xmlNsPtr ns, xmlNodeSetPtr inference_nodeset, struct VOEvent_Inference *voevent_inference){

    int k;

    for (k = 0; k < inference_nodeset->nodeNr; k++) {
        voevent_inference[k].Name        = NULL;
        voevent_inference[k].Concept     = NULL;
        voevent_inference[k].Description = NULL;
        voevent_inference[k].Reference   = NULL;

        voevent_inference[k].probability = xmlGetProp(inference_nodeset->nodeTab[k], (const xmlChar *) "probability");
        voevent_inference[k].relation    = xmlGetProp(inference_nodeset->nodeTab[k], (const xmlChar *) "relation");

        int no_table = snprintf(NULL, 0, "%d", k+1);

        char *xpath_inference_name = calloc(1, (22+no_table+1));
        sprintf(xpath_inference_name, "//Why/Inference[%d]/Name", k+1);

        xmlXPathObjectPtr inference_name = getnodeset(doc, (xmlChar *)xpath_inference_name);

        if(inference_name){
            int nr_name = 0;
            int w;
            xmlNodeSetPtr nodeset_inference_name = inference_name->nodesetval;
            nr_name = nodeset_inference_name->nodeNr;
            voevent_inference[k].nr_name = nr_name;
            voevent_inference[k].Name = calloc(nr_name, sizeof(struct VOEvent_Name));

            for(w = 0; w < nr_name; w++){
                voevent_inference[k].Name[w].name = xmlNodeListGetString(doc, nodeset_inference_name->nodeTab[w]->xmlChildrenNode, 1);
            }

            xmlXPathFreeObject(inference_name);
        }

        free(xpath_inference_name);

        char *xpath_inference_concept = calloc(1, (25+no_table+1));
        sprintf(xpath_inference_concept, "//Why/Inference[%d]/Concept", k+1);

        xmlXPathObjectPtr inference_concept = getnodeset(doc, (xmlChar *)xpath_inference_concept);

        if(inference_concept){
            int nr_concept = 0;
            int w;
            xmlNodeSetPtr nodeset_inference_concept = inference_concept->nodesetval;
            nr_concept = nodeset_inference_concept->nodeNr;
            voevent_inference[k].nr_concept = nr_concept;
            voevent_inference[k].Concept = calloc(nr_concept, sizeof(struct VOEvent_Concept));

            for(w = 0; w < nr_concept; w++){
                voevent_inference[k].Concept[w].concept = xmlNodeListGetString(doc, nodeset_inference_concept->nodeTab[w]->xmlChildrenNode, 1);
            }

            xmlXPathFreeObject(inference_concept);
        }

        free(xpath_inference_concept);

        char *xpath_inference_description = calloc(1, (29+no_table+1));
        sprintf(xpath_inference_description, "//Why/Inference[%d]/Description", k+1);

        xmlXPathObjectPtr inference_description = getnodeset(doc, (xmlChar *)xpath_inference_description);

        if(inference_description){
            int nr_description = 0;
            int j;
            xmlNodeSetPtr nodeset_inference_description = inference_description->nodesetval;
            nr_description = nodeset_inference_description->nodeNr;
            voevent_inference[k].nr_description = nr_description;
            voevent_inference[k].Description = calloc(nr_description, sizeof(struct VOEvent_Description));

            for(j = 0; j < nr_description; j++){
                parse_description(doc, ns, nodeset_inference_description->nodeTab[j], &(voevent_inference[k].Description[j]));
            }

            xmlXPathFreeObject(inference_description);
        }

        free(xpath_inference_description);

        char *xpath_inference_reference = calloc(1, (27+no_table+1));
        sprintf(xpath_inference_reference, "//Why/Inference[%d]/Reference", k+1);

        xmlXPathObjectPtr inference_reference = getnodeset(doc, (xmlChar *)xpath_inference_reference);

        if(inference_reference){
            int nr_reference = 0;
            int w;
            xmlNodeSetPtr nodeset_inference_reference = inference_reference->nodesetval;
            nr_reference = nodeset_inference_reference->nodeNr;
            voevent_inference[k].nr_reference = nr_reference;
            voevent_inference[k].Reference = calloc(nr_reference, sizeof(struct VOEvent_Reference));

            for(w = 0; w < nr_reference; w++){
                parse_reference(doc, ns, nodeset_inference_reference->nodeTab[w], &(voevent_inference[k].Reference[w]));
            }

            xmlXPathFreeObject(inference_reference);
        }

        free(xpath_inference_reference);

    }

}

/**
 * @brief      Parse a Why element of a VOEvent
 *
 * @param[in]  doc          The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns           XML Namespace
 * @param[in]  why_node     The node corresponding to the Why element
 * @param      voevent_why  A pointer to a VOEvent_Why structure
 */
void parse_why(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr why_node, struct VOEvent_Why *voevent_why){

    voevent_why->importance = xmlGetProp(why_node, (const xmlChar *) "importance");
    voevent_why->expires = xmlGetProp(why_node, (const xmlChar *) "expires");

    // set the elements to NULL in order to not have problems when freeing the structure
    voevent_why->Name        = NULL;
    voevent_why->Concept     = NULL;
    voevent_why->Inference   = NULL;
    voevent_why->Description = NULL;
    voevent_why->Reference   = NULL;

    xmlXPathObjectPtr name;
    xmlXPathObjectPtr concept;
    xmlXPathObjectPtr inference;
    xmlXPathObjectPtr description;
    xmlXPathObjectPtr reference;

    name        = getnodeset(doc, (xmlChar *)"//Why/Name");
    concept     = getnodeset(doc, (xmlChar *)"//Why/Concept");
    inference   = getnodeset(doc, (xmlChar *)"//Why/Inference");
    reference   = getnodeset(doc, (xmlChar *)"//Why/Reference");
    description = getnodeset(doc, (xmlChar *)"//Why/Description");

    xmlNodeSetPtr nodeset_name;
    xmlNodeSetPtr nodeset_concept;
    xmlNodeSetPtr nodeset_inference;
    xmlNodeSetPtr nodeset_reference;
    xmlNodeSetPtr nodeset_description;

    if(name){
        int nr_name = 0;
        int j = 0;
        nodeset_name = name->nodesetval;
        nr_name      = nodeset_name->nodeNr;
        voevent_why->nr_name = nr_name;
        voevent_why->Name = calloc(nr_name, sizeof(struct VOEvent_Name));
        for (j = 0; j < nr_name; j++) {
            voevent_why->Name[j].name = xmlNodeListGetString(doc, nodeset_name->nodeTab[j]->xmlChildrenNode, 1);
        }
        xmlXPathFreeObject(name);
    }

    if(concept){
        int nr_concept = 0;
        int j = 0;
        nodeset_concept = concept->nodesetval;
        nr_concept      = nodeset_concept->nodeNr;
        voevent_why->nr_concept = nr_concept;
        voevent_why->Concept = calloc(nr_concept, sizeof(struct VOEvent_Concept));
        for (j = 0; j < nr_concept; j++) {
            voevent_why->Concept[j].concept = xmlNodeListGetString(doc, nodeset_concept->nodeTab[j]->xmlChildrenNode, 1);
        }
        xmlXPathFreeObject(concept);
    }

    if(inference){
        int nr_inference = 0;
        nodeset_inference = inference->nodesetval;
        nr_inference      = nodeset_inference->nodeNr;
        voevent_why->nr_inference = nr_inference;
        voevent_why->Inference = calloc(nr_inference, sizeof(struct VOEvent_Inference));

        parse_inference(doc, ns, nodeset_inference, voevent_why->Inference);

        xmlXPathFreeObject(inference);
    }

    if(reference){
        int nr_reference = 0;
        int j = 0;
        nodeset_reference = reference->nodesetval;
        nr_reference      = nodeset_reference->nodeNr;
        voevent_why->nr_reference = nr_reference;
        voevent_why->Reference = calloc(nr_reference, sizeof(struct VOEvent_Reference));
        for (j = 0; j < nr_reference; j++) {
            parse_reference(doc, ns, nodeset_reference->nodeTab[j], &(voevent_why->Reference[j]));
        }
        xmlXPathFreeObject(reference);
    }

    if(description){
        int nr_description = 0;
        int k = 0;
        nodeset_description = description->nodesetval;
        nr_description      = nodeset_description->nodeNr;
        voevent_why->nr_description = nr_description;
        voevent_why->Description = calloc(nr_description, sizeof(struct VOEvent_Description));
        for (k = 0; k < nr_description; k++) {
            parse_description(doc, ns, nodeset_description->nodeTab[k], &(voevent_why->Description[k]));
        }
        xmlXPathFreeObject(description);
    }

    return;
}

/**
 * @brief      Parses a VOEvent contained in an XML file.
 *
 * Parse the voevent and fill all the structures
 * VOEvent *voevent should be passed as an argument.
 * When you use this function, the code looks like this
 * in your main function or elsewhere:
 *
 * \code{.c}
 * xmlInitParser(); // initialize XML library
 * struct VOEvent voevent = {0};
 *
 * // filename argument is supplied somewhere e.g. command line
 * // argument or as a string
 * // here we suppose filename was copied from argv[1]
 *
 * char *filename = (char *) malloc(strlen(argv[1])+1);
 * strcpy(filename, argv[1]);
 *
 * int parsed = parse_voevent_xmlfile(filename, &voevent);
 *
 * // check if parsing is successful
 * // if parsing is unsuccessful, the user must free the VOEvent structure!!!
 * if (parsed != 0){
 *    free_voevent(&voevent);
 *    free(filename);
 *    xmlCleanupParser(); // cleanup XML library
 *    return 1;
 * }
 *
 * //.....
 * //do stuff with voevent
 * //.....
 *
 * free_voevent(&voevent); // to free the memory allocated in filling voevent
 * xmlCleanupParser(); // cleanup XML library
 * \endcode
 *
 * @param[in]  filename  The filename of the VOEvent XML file
 * @param      voevent   A pointer to a VOEvent structure
 *
 * @return     0 if parsing is succesfull, 1 otherwise
 */
int parse_voevent_xmlfile(const char *filename, struct VOEvent *voevent) {
    xmlNsPtr ns;
    xmlNodePtr root_node;
    xmlNodePtr cur;

    // validate VOEvent schema
    int valid_schema = validate_voevent_xmlfile(filename, VOEventSchemaFile);
    if(valid_schema){
        fprintf(stderr, "%s does not validate. Exiting.\n", filename);
        return 1;
    }

    #ifdef LIBXML_SAX1_ENABLED
        //build an XML tree from a the file;
        voevent->Doc = xmlParseFile(filename);
        if (voevent->Doc == NULL){
            return 1;
        }
    #else
        fprintf(stderr, "XML library has been compiled without some of the old interfaces. Exiting\n");
        return 1;
    #endif //LIBXML_SAX1_ENABLED

    // Check if the document is of the right kind
    
    root_node = xmlDocGetRootElement(voevent->Doc);
    if (root_node == NULL) {
        fprintf(stderr,"%s is an empty document. Exiting.\n", filename);
        return 1;
    }

    ns = xmlSearchNsByHref(voevent->Doc, root_node, (const xmlChar *) "http://www.ivoa.net/xml/VOEvent/v2.0");
    if (ns == NULL) {
        fprintf(stderr, "%s is a document of the wrong type, VOEvent Namespace not found. Exiting.\n", filename);
        return 1;
    }

    if (xmlStrcmp(root_node->name, (const xmlChar *) "VOEvent")) {
        fprintf(stderr,"%s is a document of the wrong type, root node != VOEvent", filename);
        return 1;
    }

    // allocate memory for VOEvent_root member of VOEvent

    voevent->VOEvent = (struct VOEvent_root *) malloc(sizeof(struct VOEvent_root));

    if (voevent->VOEvent == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_root structure. Exiting.\n");
        return 1;
    }

    memset(voevent->VOEvent, 0, sizeof(struct VOEvent_root));

    parse_root(voevent->Doc, ns, root_node, voevent->VOEvent);

    //Now, walk the tree.
    cur = root_node->xmlChildrenNode;

    if (cur==0) {
        printf("Root node has no children nodes. Exiting.\n");
        return 1;
    }

    // allocate memory for the structures composing VOEvent structure
    // if one of the allocation fails, the functions returns 1
    // and the user must use the function free_voevent defined in
    // voevent_cleanup.c to free the memeory allocated up to that point

    voevent->Who = (struct VOEvent_Who *) malloc(sizeof(struct VOEvent_Who));

    if (voevent->Who == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_Who structure. Exiting.\n");
        return 1;
    }

    memset(voevent->Who, 0, sizeof(struct VOEvent_Who));

    voevent->What = (struct VOEvent_What *) malloc(sizeof(struct VOEvent_What));

    if (voevent->What == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_What structure. Exiting.\n");
        return 1;
    }

    memset(voevent->What, 0, sizeof(struct VOEvent_What));

    voevent->How = (struct VOEvent_How *) malloc(sizeof(struct VOEvent_How));

    if (voevent->How == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_How structure. Exiting.\n");
        return 1;
    }

    memset(voevent->How, 0, sizeof(struct VOEvent_How));

    voevent->WhereWhen = (struct VOEvent_WhereWhen *) malloc(sizeof(struct VOEvent_WhereWhen));

    if (voevent->WhereWhen == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_WhereWhen structure. Exiting.\n");
        return 1;
    }

    memset(voevent->WhereWhen, 0, sizeof(struct VOEvent_WhereWhen));
    
    voevent->Citations = (struct VOEvent_Citations *) malloc(sizeof(struct VOEvent_Citations));

    if (voevent->Citations == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_Citations structure. Exiting.\n");
        return 1;
    }

    memset(voevent->Citations, 0, sizeof(struct VOEvent_Citations));

    voevent->Description = (struct VOEvent_Description *) malloc(sizeof(struct VOEvent_Description));

    if (voevent->Description == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_Description structure. Exiting.\n");
        return 1;
    }

    memset(voevent->Description, 0, sizeof(struct VOEvent_Description));

    voevent->Why = (struct VOEvent_Why *) malloc(sizeof(struct VOEvent_Why));

    if (voevent->Why == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_Why structure. Exiting.\n");
        return 1;
    }

    memset(voevent->Why, 0, sizeof(struct VOEvent_Why));

    voevent->Reference = (struct VOEvent_Reference *) malloc(sizeof(struct VOEvent_Reference));

    if (voevent->Reference == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_Reference structure. Exiting.\n");
        return 1;
    }

    memset(voevent->Reference, 0, sizeof(struct VOEvent_Reference));

    while (cur != NULL){

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Who"))){
            parse_who(voevent->Doc, ns, cur, voevent->Who);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"What"))){
            parse_what(voevent->Doc, ns, cur, voevent->What);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"WhereWhen"))){
            parse_wherewhen(voevent->Doc, ns, cur, voevent->WhereWhen);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"How"))){
            parse_how(voevent->Doc, ns, cur, voevent->How);
        }
        
        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Citations"))){
            parse_citations(voevent->Doc, ns, cur, voevent->Citations);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Description"))){
            parse_description(voevent->Doc, ns, cur, voevent->Description);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Why"))){
            parse_why(voevent->Doc, ns, cur, voevent->Why);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Reference"))){
            parse_reference(voevent->Doc, ns, cur, voevent->Reference);
        }

        cur = cur->next;
    }

    //if ((xmlStrcmp(cur->name, (const xmlChar *) "")) || (cur->ns != ns)) {
    //   fprintf(stderr,"document of the wrong type, was '%s', Jobs expected",
    //   cur->name);
    //   fprintf(stderr,"xmlDocDump follows\n");
    //#ifdef LIBXML_OUTPUT_ENABLED
    //   xmlDocDump ( stderr, doc );
    //   fprintf(stderr,"xmlDocDump finished\n");
    //#endif /* LIBXML_OUTPUT_ENABLED */
    //
    //xmlFreeDoc(doc);
    //free(who);
//
    //return;
    //}
    //

    return 0;
}

/**
 * @brief      Parses a VOEvent contained in string buffer.
 *
 * Parse the voevent and fill all the structures
 * VOEvent *voevent should be passed as an argument.
 * When you use this function, the code looks like this
 * in your main function or elsewhere:
 *
 * \code{.c}
 * xmlInitParser(); // initialize XML library
 * struct VOEvent voevent = {0};
 *
 * // buffer contains the XML document, and has size "size"
 *
 * int parsed = parse_voevent_xmlmemory(buffer, size, &voevent);
 *
 * // check if parsing is successful
 * // if parsing is unsuccessful, the user must free the VOEvent structure!!!
 * if (parsed != 0){
 *    free_voevent(&voevent);
 *    xmlCleanupParser(); // cleanup XML library
 *    return 1;
 * }
 *
 * //.....
 * //do stuff with voevent
 * //.....
 *
 * free_voevent(&voevent); // to free the memory allocated in filling voevent
 * xmlCleanupParser(); // cleanup XML library
 * \endcode
 *
 * @param[in]  buffer    The string buffer
 * @param[in]  size      The string buffer size
 * @param      voevent   A pointer to a VOEvent structure
 *
 * @return     0 if parsing is succesfull, 1 otherwise
 */
int parse_voevent_xmlmemory(const char *buffer, const long int size, struct VOEvent *voevent) {
    xmlNsPtr ns;
    xmlNodePtr root_node;
    xmlNodePtr cur;

    // validate VOEvent schema
    int valid_schema = validate_voevent_xmlmemory(buffer, size, VOEventSchemaFile);
    if(valid_schema){
        fprintf(stderr, "VOEvent does not validate. Exiting.\n");
        return 1;
    }

    #ifdef LIBXML_SAX1_ENABLED
        //build an XML tree from a the file;
        voevent->Doc = xmlParseMemory(buffer, size);
        if (voevent->Doc == NULL){
            return 1;
        }
    #else
        fprintf(stderr, "XML library has been compiled without some of the old interfaces. Exiting\n");
        return 1;
    #endif //LIBXML_SAX1_ENABLED

    // Check if the document is of the right kind

    root_node = xmlDocGetRootElement(voevent->Doc);
    if (root_node == NULL) {
        fprintf(stderr,"Buffer is an empty document. Exiting.\n");
        return 1;
    }

    ns = xmlSearchNsByHref(voevent->Doc, root_node, (const xmlChar *) "http://www.ivoa.net/xml/VOEvent/v2.0");
    if (ns == NULL) {
        fprintf(stderr, "Buffer is a document of the wrong type, VOEvent Namespace not found. Exiting.\n");
        return 1;
    }

    if (xmlStrcmp(root_node->name, (const xmlChar *) "VOEvent")) {
        fprintf(stderr,"Buffer is a document of the wrong type, root node != VOEvent");
        return 1;
    }

    // allocate memory for VOEvent_root member of VOEvent

    voevent->VOEvent = (struct VOEvent_root *) malloc(sizeof(struct VOEvent_root));

    if (voevent->VOEvent == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_root structure. Exiting.\n");
        return 1;
    }

    memset(voevent->VOEvent, 0, sizeof(struct VOEvent_root));

    parse_root(voevent->Doc, ns, root_node, voevent->VOEvent);

    //Now, walk the tree.
    cur = root_node->xmlChildrenNode;

    if (cur==0) {
        printf("Root node has no children nodes. Exiting.\n");
        return 1;
    }

    // allocate memory for the structures composing VOEvent structure
    // if one of the allocation fails, the functions returns 1
    // and the user must use the function free_voevent defined in
    // voevent_cleanup.c to free the memeory allocated up to that point

    voevent->Who = (struct VOEvent_Who *) malloc(sizeof(struct VOEvent_Who));

    if (voevent->Who == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_Who structure. Exiting.\n");
        return 1;
    }

    memset(voevent->Who, 0, sizeof(struct VOEvent_Who));

    voevent->What = (struct VOEvent_What *) malloc(sizeof(struct VOEvent_What));

    if (voevent->What == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_What structure. Exiting.\n");
        return 1;
    }

    memset(voevent->What, 0, sizeof(struct VOEvent_What));

    voevent->How = (struct VOEvent_How *) malloc(sizeof(struct VOEvent_How));

    if (voevent->How == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_How structure. Exiting.\n");
        return 1;
    }

    memset(voevent->How, 0, sizeof(struct VOEvent_How));

    voevent->WhereWhen = (struct VOEvent_WhereWhen *) malloc(sizeof(struct VOEvent_WhereWhen));

    if (voevent->WhereWhen == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_WhereWhen structure. Exiting.\n");
        return 1;
    }

    memset(voevent->WhereWhen, 0, sizeof(struct VOEvent_WhereWhen));
    
    voevent->Citations = (struct VOEvent_Citations *) malloc(sizeof(struct VOEvent_Citations));

    if (voevent->Citations == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_Citations structure. Exiting.\n");
        return 1;
    }

    memset(voevent->Citations, 0, sizeof(struct VOEvent_Citations));

    voevent->Description = (struct VOEvent_Description *) malloc(sizeof(struct VOEvent_Description));

    if (voevent->Description == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_Description structure. Exiting.\n");
        return 1;
    }

    memset(voevent->Description, 0, sizeof(struct VOEvent_Description));

    voevent->Why = (struct VOEvent_Why *) malloc(sizeof(struct VOEvent_Why));

    if (voevent->Why == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_Why structure. Exiting.\n");
        return 1;
    }

    memset(voevent->Why, 0, sizeof(struct VOEvent_Why));

    voevent->Reference = (struct VOEvent_Reference *) malloc(sizeof(struct VOEvent_Reference));

    if (voevent->Reference == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for VOEvent_Reference structure. Exiting.\n");
        return 1;
    }

    memset(voevent->Reference, 0, sizeof(struct VOEvent_Reference));

    while (cur != NULL){

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Who"))){
            parse_who(voevent->Doc, ns, cur, voevent->Who);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"What"))){
            parse_what(voevent->Doc, ns, cur, voevent->What);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"WhereWhen"))){
            parse_wherewhen(voevent->Doc, ns, cur, voevent->WhereWhen);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"How"))){
            parse_how(voevent->Doc, ns, cur, voevent->How);
        }
        
        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Citations"))){
            parse_citations(voevent->Doc, ns, cur, voevent->Citations);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Description"))){
            parse_description(voevent->Doc, ns, cur, voevent->Description);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Why"))){
            parse_why(voevent->Doc, ns, cur, voevent->Why);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Reference"))){
            parse_reference(voevent->Doc, ns, cur, voevent->Reference);
        }

        cur = cur->next;
    }

    //if ((xmlStrcmp(cur->name, (const xmlChar *) "")) || (cur->ns != ns)) {
    //   fprintf(stderr,"document of the wrong type, was '%s', Jobs expected",
    //   cur->name);
    //   fprintf(stderr,"xmlDocDump follows\n");
    //#ifdef LIBXML_OUTPUT_ENABLED
    //   xmlDocDump ( stderr, doc );
    //   fprintf(stderr,"xmlDocDump finished\n");
    //#endif /* LIBXML_OUTPUT_ENABLED */
    //
    //xmlFreeDoc(doc);
    //free(who);
//
    //return;
    //}
    //

    return 0;
}

/**
 * @brief      Parse the root element of a Transport message
 *
 * @param[in]  doc             The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns              XML Namespace
 * @param[in]  root_node       The node corresponding to the root element of the Transport
 * @param      transport_root  A pointer to a Transport_root structure
 */
void parse_root_transport(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr root_node, struct Transport_root *transport_root) {

    // get Transport version and role. They MUST be there, otherwise the document would not
    // be valid. Cleanup should be performed somewhere since xmlGetProp allocates memory.

    transport_root->version = xmlGetProp(root_node, (const xmlChar *) "version");
    transport_root->role    = xmlGetProp(root_node, (const xmlChar *) "role");

    return;
}

/**
 * @brief      Parse a Meta element of a Transport message
 *
 * @param[in]  doc             The pointer to the structure containing the tree of the parsed document
 * @param[in]  ns              XML Namespace
 * @param[in]  meta_node       The node corresponding to the Meta element
 * @param      transport_meta  A pointer to a Transport_Meta structure
 */
void parse_meta_transport(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr meta_node, struct Transport_Meta *transport_meta){
    // set the elements to NULL in order to not have problems when freeing the structure
    xmlNodePtr cur;
    transport_meta->meta_param  = NULL;
    transport_meta->result      = NULL;

    xmlXPathObjectPtr param = getnodeset(doc, (xmlChar *)"//Meta/Param");

    if(param){
        int nr_params = 0;
        xmlNodeSetPtr nodeset_param = param->nodesetval;
        nr_params     = nodeset_param->nodeNr;
        transport_meta->nr_params = nr_params;
        printf("%d\n", nr_params);
        transport_meta->meta_param = calloc(nr_params, sizeof(struct VOEvent_Param));

        parse_param(doc, ns, nodeset_param, transport_meta->meta_param);
        xmlXPathFreeObject(param);
    }

    cur = meta_node->xmlChildrenNode;
    while (cur != NULL) {

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Result")){
            transport_meta->result = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        }

        cur = cur->next;
    }

    return;
}

/**
 * @brief      Parses a Transport message contained in an XML file.
 *
 * @param[in]  filename    The filename of the Transport XML file
 * @param      transport   A pointer to a Transport structure
 *
 * @return     0 if parsing is succesfull, 1 otherwise
 */
int parse_transport_xmlfile(const char *filename, struct Transport *transport) {
    xmlNsPtr ns;
    xmlNodePtr root_node;
    xmlNodePtr cur;

    //validate VOEvent schema
    int valid_schema = validate_voevent_xmlfile(filename, TransportSchemaFile);
    if(valid_schema){
        fprintf(stderr, "%s does not validate. Exiting.\n", filename);
        return 1;
    }

    #ifdef LIBXML_SAX1_ENABLED
        //build an XML tree from a the file;
        //xmlInitParser();
        transport->Doc = xmlParseFile(filename);
        if (transport->Doc == NULL){
            return 1;
        }
    #else
        //the library has been compiled without some of the old interfaces
        return 1;
    #endif //LIBXML_SAX1_ENABLED

    // Check if the document is of the right kind

    root_node = xmlDocGetRootElement(transport->Doc);
    if (root_node == NULL) {
        fprintf(stderr,"%s is an empty document. Exiting.\n", filename);
        return 1;
    }

    ns = xmlSearchNsByHref(transport->Doc, root_node, (const xmlChar *) "http://www.w3.org/2001/XMLSchema-instance");
    if (ns == NULL) {
        fprintf(stderr, "%s is a document of the wrong type, Transport Namespace not found. Exiting.\n", filename);
        return 1;
    }

    if (xmlStrcmp(root_node->name, (const xmlChar *) "Transport")) {
        fprintf(stderr,"%s is a document of the wrong type, root node != Transport", filename);
        return 1;
    }

    // allocate memory for Transport_root member of Transport

    transport->Transport = (struct Transport_root *) malloc(sizeof(struct Transport_root));

    if (transport->Transport == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for Transport_root structure. Exiting.\n");
        return 1;
    }

    memset(transport->Transport, 0, sizeof(struct Transport_root));

    parse_root_transport(transport->Doc, ns, root_node, transport->Transport);

    //Now, walk the tree.
    cur = root_node->xmlChildrenNode;

    if (cur==0) {
        printf("Root node has no children nodes. Exiting.\n");
        return 1;
    }

    transport->Meta = (struct Transport_Meta *) malloc(sizeof(struct Transport_Meta));

    if (transport->Meta == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for Transport_Meta structure. Exiting.\n");
        return 1;
    }

    memset(transport->Meta, 0, sizeof(struct Transport_Meta));

    transport->Origin    = NULL;
    transport->TimeStamp = NULL;
    transport->Response  = NULL;

    while (cur != NULL){

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Origin")){
            transport->Origin = xmlNodeListGetString(transport->Doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "TimeStamp")){
            transport->TimeStamp = xmlNodeListGetString(transport->Doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Response")){
            transport->TimeStamp = xmlNodeListGetString(transport->Doc, cur->xmlChildrenNode, 1);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Meta"))){
            parse_meta_transport(transport->Doc, ns, cur, transport->Meta);
        }

        cur = cur->next;
    }

    return 0;
}

/**
 * @brief      Parses a Transport message contained in a string buffer.
 *
 * @param[in]  buffer    The string buffer
 * @param[in]  size      The string buffer size
 * @param      transport   A pointer to a Transport structure
 *
 * @return     0 if parsing is succesfull, 1 otherwise
 */
int parse_transport_xmlmemory(const char *buffer, const long int size, struct Transport *transport) {
    xmlNsPtr ns;
    xmlNodePtr root_node;
    xmlNodePtr cur;

    //validate VOEvent schema
    int valid_schema = validate_voevent_xmlmemory(buffer, size, TransportSchemaFile);
    if(valid_schema){
        fprintf(stderr, "VOEvent does not validate. Exiting.\n");
        return 1;
    }

    #ifdef LIBXML_SAX1_ENABLED
        //build an XML tree from a the file;
        //xmlInitParser();
        transport->Doc = xmlParseMemory(buffer, size);
        if (transport->Doc == NULL){
            return 1;
        }
    #else
        //the library has been compiled without some of the old interfaces
        return 1;
    #endif //LIBXML_SAX1_ENABLED

    // Check if the document is of the right kind

    root_node = xmlDocGetRootElement(transport->Doc);
    if (root_node == NULL) {
        fprintf(stderr,"Buffer is an empty document. Exiting.\n");
        return 1;
    }

    ns = xmlSearchNsByHref(transport->Doc, root_node, (const xmlChar *) "http://www.w3.org/2001/XMLSchema-instance");
    if (ns == NULL) {
        fprintf(stderr, "Buffer is a document of the wrong type, Transport Namespace not found. Exiting.\n");
        return 1;
    }

    if (xmlStrcmp(root_node->name, (const xmlChar *) "Transport")) {
        fprintf(stderr,"Buffer is a document of the wrong type, root node != Transport");
        return 1;
    }

    // allocate memory for Transport_root member of Transport

    transport->Transport = (struct Transport_root *) malloc(sizeof(struct Transport_root));

    if (transport->Transport == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for Transport_root structure. Exiting.\n");
        return 1;
    }

    memset(transport->Transport, 0, sizeof(struct Transport_root));

    parse_root_transport(transport->Doc, ns, root_node, transport->Transport);

    //Now, walk the tree.
    cur = root_node->xmlChildrenNode;

    if (cur==0) {
        printf("Root node has no children nodes. Exiting.\n");
        return 1;
    }

    transport->Meta = (struct Transport_Meta *) malloc(sizeof(struct Transport_Meta));

    if (transport->Meta == NULL) {
        fprintf(stderr,"Out of memory: cannot allocate memory for Transport_Meta structure. Exiting.\n");
        return 1;
    }

    memset(transport->Meta, 0, sizeof(struct Transport_Meta));

    transport->Origin    = NULL;
    transport->TimeStamp = NULL;
    transport->Response  = NULL;

    while (cur != NULL){

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Origin")){
            transport->Origin = xmlNodeListGetString(transport->Doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "TimeStamp")){
            transport->TimeStamp = xmlNodeListGetString(transport->Doc, cur->xmlChildrenNode, 1);
        }

        if (!xmlStrcmp(cur->name, (const xmlChar *) "Response")){
            transport->TimeStamp = xmlNodeListGetString(transport->Doc, cur->xmlChildrenNode, 1);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Meta"))){
            parse_meta_transport(transport->Doc, ns, cur, transport->Meta);
        }

        cur = cur->next;
    }

    return 0;
}
