/**
 * @file voevent_cleanup.c
 * @brief Source file for the the cleanup of memory.
 *
 */

#include <stdio.h>
#include <libxml/parser.h>
#include "voevent.h"

/**
 * @brief      Free the VOEvent root element
 *
 * @param      voevent  A pointer to a VOEvent structure
 */
void free_voevent_root(struct VOEvent *voevent){

    if((voevent->VOEvent)->version){
        #ifdef DEBUG
            printf("Freeing version\n");
        #endif
        xmlFree((voevent->VOEvent)->version);
    }

    if((voevent->VOEvent)->role){
        #ifdef DEBUG
            printf("Freeing role\n");
        #endif
        xmlFree((voevent->VOEvent)->role);
    }

    if((voevent->VOEvent)->ivorn){
        #ifdef DEBUG
            printf("Freeing ivorn\n");
        #endif
        xmlFree((voevent->VOEvent)->ivorn);
    }

    free(voevent->VOEvent);

}

/**
 * @brief      Free the VOEvent Author element
 *
 * @param      voevent_author  A pointer to a VOEvent_Author structure
 */
void free_voevent_author(struct VOEvent_Author *voevent_author){

#ifdef DEBUG
        printf("Freeing author\n");
    #endif

    if(voevent_author->title){
        #ifdef DEBUG
            printf("Freeing author title\n");
        #endif
        xmlFree(voevent_author->title);
    }

    if(voevent_author->shortName){
        #ifdef DEBUG
            printf("Freeing author shortName\n");
        #endif
        xmlFree(voevent_author->shortName);
    }

    if(voevent_author->logoURL){
        #ifdef DEBUG
            printf("Freeing author logoURL\n");
        #endif
        xmlFree(voevent_author->logoURL);
    }

    if(voevent_author->contactName){
        #ifdef DEBUG
            printf("Freeing author contactName\n");
        #endif
        xmlFree(voevent_author->contactName);
    }

    if(voevent_author->contactEmail){
        #ifdef DEBUG
            printf("Freeing author contactEmail\n");
        #endif
        xmlFree(voevent_author->contactEmail);
    }

    if(voevent_author->contactPhone){
        #ifdef DEBUG
            printf("Freeing author contactPhone\n");
        #endif
        xmlFree(voevent_author->contactPhone);
    }

    if(voevent_author->contributor){
        #ifdef DEBUG
            printf("Freeing author contributor\n");
        #endif
        xmlFree(voevent_author->contributor);
    }

}

/**
 * @brief      Free the VOEvent Reference element
 *
 * @param      voevent_reference  A pointer to a VOEvent_Reference structure
 */
void free_voevent_reference(struct VOEvent_Reference *voevent_reference){
    if(voevent_reference->uri){
        #ifdef DEBUG
            printf("Freeing reference uri\n");
        #endif
        xmlFree(voevent_reference->uri);
    }

    if(voevent_reference->type){
        #ifdef DEBUG
            printf("Freeing reference type\n");
        #endif
        xmlFree(voevent_reference->type);
    }

    if(voevent_reference->mimetype){
        #ifdef DEBUG
            printf("Freeing reference mimetype\n");
        #endif
        xmlFree(voevent_reference->mimetype);
    }

    if(voevent_reference->meaning){
        #ifdef DEBUG
            printf("Freeing reference meaning\n");
        #endif
        xmlFree(voevent_reference->meaning);
    }
}

/**
 * @brief      Free the VOEvent Description element
 *
 * @param      voevent_description  A pointer to a VOEvent_Description structure
 */
void free_voevent_description(struct VOEvent_Description *voevent_description){
    if(voevent_description->description){
        #ifdef DEBUG
            printf("Freeing description\n");
        #endif
        xmlFree(voevent_description->description);
    }
}

/**
 * @brief      Free the VOEvent Who element
 *
 * @param      voevent  A pointer to a VOEvent structure
 */
void free_voevent_who(struct VOEvent *voevent){
    if((voevent->Who)->AuthorIVORN){
        #ifdef DEBUG
            printf("Freeing who author ivorn node\n");
        #endif
        xmlFree((voevent->Who)->AuthorIVORN);
    }

    if((voevent->Who)->Date){
        #ifdef DEBUG
            printf("Freeing who date node\n");
        #endif
        xmlFree((voevent->Who)->Date);
    }

    if((voevent->Who)->Author){
        #ifdef DEBUG
            printf("Freeing who author node\n");
        #endif
        free_voevent_author((voevent->Who)->Author);
        free((voevent->Who)->Author);
    }

    if((voevent->Who)->Reference){
        #ifdef DEBUG
            printf("Freeing who reference node\n");
        #endif
        free_voevent_reference((voevent->Who)->Reference);
        free((voevent->Who)->Reference);
    }

    if((voevent->Who)->Description){
        #ifdef DEBUG
            printf("Freeing who description node\n");
        #endif
        free_voevent_description((voevent->Who)->Description);
        free((voevent->Who)->Description);
    }

    free(voevent->Who);
}

/**
 * @brief      Free the VOEvent Param element
 *
 * @param      param  A pointer to a VOEvent_Param structure
 */
void free_voevent_param(struct VOEvent_Param *param){
    if(param->Description){
        int j;
        for (j = 0; j < param->nr_description; j++){
            free_voevent_description(&(param->Description[j]));
        }
        free(param->Description);
    }

    if(param->Reference){
        int j;
        for (j = 0; j < param->nr_reference; j++){
            free_voevent_reference(&(param->Reference[j]));
        }
        free(param->Reference);
    }

    if(param->name){
        xmlFree(param->name);
    }

    if(param->ucd){
        xmlFree(param->ucd);
    }

    if(param->value){
        xmlFree(param->value);
    }

    if(param->unit){
        xmlFree(param->unit);
    }

    if(param->dataType){
        xmlFree(param->dataType);
    }

    if(param->utype){
        xmlFree(param->utype);
    }

    if(param->Value){
        xmlFree(param->Value);
    }

}

/**
 * @brief      Free the VOEvent Group element
 *
 * @param      group  A pointer to a VOEvent_Group structure
 */
void free_voevent_group(struct VOEvent_Group *group){
    if(group->Description){
        int j;
        for (j = 0; j < group->nr_description; j++){
            free_voevent_description(&(group->Description[j]));
        }
        free(group->Description);
    }

    if(group->Reference){
        int j;
        for (j = 0; j < group->nr_reference; j++){
            free_voevent_reference(&(group->Reference[j]));
        }
        free(group->Reference);
    }

    if(group->Param){
        int j;
        for (j = 0; j < group->nr_params; j++){
            free_voevent_param(&(group->Param[j]));
        }
        free(group->Param);
    }

    if(group->name){
        xmlFree(group->name);
    }

    if(group->type){
        xmlFree(group->type);
    }

}

/**
 * @brief      Free the VOEvent Field element
 *
 * @param      field  A pointer to a VOEvent_Field structure
 */
void free_voevent_field(struct VOEvent_Field *field){
    if(field->Description){
        int j;
        for (j = 0; j < field->nr_description; j++){
            free_voevent_description(&(field->Description[j]));
        }
        free(field->Description);
    }

    if(field->Reference){
        int j;
        for (j = 0; j < field->nr_reference; j++){
            free_voevent_reference(&(field->Reference[j]));
        }
        free(field->Reference);
    }

    if(field->name){
        xmlFree(field->name);
    }

    if(field->ucd){
        xmlFree(field->ucd);
    }

    if(field->unit){
        xmlFree(field->unit);
    }

    if(field->dataType){
        xmlFree(field->dataType);
    }

    if(field->utype){
        xmlFree(field->utype);
    }

}

/**
 * @brief      Free the VOEvent Data element
 *
 * @param      data  A pointer to a VOEvent_Data structure
 */
void free_voevent_data(struct VOEvent_Data *data){
    
    if(data->data){
        xmlFree(data->data);
    }
}

/**
 * @brief      Free the VOEvent Table element
 *
 * @param      table  A pointer to a VOEvent_Table structure
 */
void free_voevent_table(struct VOEvent_Table *table){
    if(table->Description){
        int j;
        for (j = 0; j < table->nr_description; j++){
            free_voevent_description(&(table->Description[j]));
        }
        free(table->Description);
    }

    if(table->Reference){
        int j;
        for (j = 0; j < table->nr_reference; j++){
            free_voevent_reference(&(table->Reference[j]));
        }
        free(table->Reference);
    }

    if(table->Param){
        int j;
        for (j = 0; j < table->nr_params; j++){
            free_voevent_param(&(table->Param[j]));
        }
        free(table->Param);
    }

    if(table->Field){
        int j;
        for (j = 0; j < table->nr_columns; j++){
            free_voevent_field(&(table->Field[j]));
        }
        free(table->Field);
    }

    if(table->Data){
        int j;
        for (j = 0; j < (table->nr_columns * table->nr_rows); j++){
            free_voevent_data(&(table->Data[j]));
        }
        free(table->Data);
    }

    if(table->name){
        xmlFree(table->name);
    }

    if(table->type){
        xmlFree(table->type);
    }

}

/**
 * @brief      Free the VOEvent What element
 *
 * @param      voevent  A pointer to a VOEvent structure
 */
void free_voevent_what(struct VOEvent *voevent){
    if((voevent->What)->Description){
        int j;
        for (j = 0; j < voevent->What->nr_description; j++){
            free_voevent_description(&(voevent->What->Description[j]));
        }
        free((voevent->What)->Description);
    }

    if((voevent->What)->Reference){
        int j;
        for (j = 0; j < voevent->What->nr_reference; j++){
            free_voevent_reference(&(voevent->What->Reference[j]));
        }
        free((voevent->What)->Reference);
    }

    if((voevent->What)->Param){
        int j;
        for (j = 0; j < voevent->What->nr_params; j++){
            free_voevent_param(&(voevent->What->Param[j]));
        }
        free((voevent->What)->Param);
    }

    if((voevent->What)->Group){
        int j;
        for (j = 0; j < voevent->What->nr_groups; j++){
            free_voevent_group(&(voevent->What->Group[j]));
        }
        free((voevent->What)->Group);
    }

    if((voevent->What)->Table){
        int j;
        for (j = 0; j < voevent->What->nr_tables; j++){
            free_voevent_table(&(voevent->What->Table[j]));
        }
        free((voevent->What)->Table);
    }

    free(voevent->What);

}

/**
 * @brief      Free the VOEvent TimeInstant element
 *
 * @param      voevent_timeinstant  A pointer to a VOEvent_TimeInstant structure
 */
void free_voevent_timeinstant(struct VOEvent_TimeInstant *voevent_timeinstant){

    if(voevent_timeinstant->ISOTime){
        #ifdef DEBUG
            printf("Freeing ISOTime element\n");
        #endif
        xmlFree(voevent_timeinstant->ISOTime);
    }

    if(voevent_timeinstant->TimeOffset){
        #ifdef DEBUG
            printf("Freeing TimeOffset element\n");
        #endif
        xmlFree(voevent_timeinstant->TimeOffset);
    }

    if(voevent_timeinstant->TimeScale){
        #ifdef DEBUG
            printf("Freeing TimeScale element\n");
        #endif
        xmlFree(voevent_timeinstant->TimeScale);
    }

}

/**
 * @brief      Free the VOEvent Time element
 *
 * @param      voevent_time  A pointer to a VOEvent_Time structure
 */
void free_voevent_time(struct VOEvent_Time *voevent_time){

    if(voevent_time->Error){
        #ifdef DEBUG
            printf("Freeing Error element\n");
        #endif
        xmlFree(voevent_time->Error);
    }

    if(voevent_time->TimeInstant){
        #ifdef DEBUG
            printf("Freeing TimeInstant element\n");
        #endif
        free_voevent_timeinstant(voevent_time->TimeInstant);
        free(voevent_time->TimeInstant);
    }  

}

/**
 * @brief      Free the VOEvent Position2D element
 *
 * @param      voevent_position2d  A pointer to a VOEvent_Position2D structure
 */
void free_voevent_position2d(struct VOEvent_Position2D *voevent_position2d){

    if(voevent_position2d->Name1){
        #ifdef DEBUG
            printf("Freeing Name1 element\n");
        #endif
        xmlFree(voevent_position2d->Name1);
    }

    if(voevent_position2d->Name2){
        #ifdef DEBUG
            printf("Freeing Name2 element\n");
        #endif
        xmlFree(voevent_position2d->Name2);
    }

    if(voevent_position2d->C1){
        #ifdef DEBUG
            printf("Freeing C1 element\n");
        #endif
        xmlFree(voevent_position2d->C1);
    }

    if(voevent_position2d->C2){
        #ifdef DEBUG
            printf("Freeing C2 element\n");
        #endif
        xmlFree(voevent_position2d->C2);
    }

    if(voevent_position2d->ErrorRadius){
        #ifdef DEBUG
            printf("Freeing ErrorRadius element\n");
        #endif
        xmlFree(voevent_position2d->ErrorRadius);
    }

    if(voevent_position2d->unit){
        #ifdef DEBUG
            printf("Freeing unit attribute\n");
        #endif
        xmlFree(voevent_position2d->unit);
    }

}

/**
 * @brief      Free the VOEvent Position3D element
 *
 * @param      voevent_position3d  A pointer to a VOEvent_Position3D structure
 */
void free_voevent_position3d(struct VOEvent_Position3D *voevent_position3d){

    if(voevent_position3d->Name1){
        #ifdef DEBUG
            printf("Freeing Name1 element\n");
        #endif
        xmlFree(voevent_position3d->Name1);
    }

    if(voevent_position3d->Name2){
        #ifdef DEBUG
            printf("Freeing Name2 element\n");
        #endif
        xmlFree(voevent_position3d->Name2);
    }

    if(voevent_position3d->Name3){
        #ifdef DEBUG
            printf("Freeing Name3 element\n");
        #endif
        xmlFree(voevent_position3d->Name3);
    }

    if(voevent_position3d->C1){
        #ifdef DEBUG
            printf("Freeing C1 element\n");
        #endif
        xmlFree(voevent_position3d->C1);
    }

    if(voevent_position3d->C2){
        #ifdef DEBUG
            printf("Freeing C2 element\n");
        #endif
        xmlFree(voevent_position3d->C2);
    }

    if(voevent_position3d->C3){
        #ifdef DEBUG
            printf("Freeing C3 element\n");
        #endif
        xmlFree(voevent_position3d->C3);
    }

    if(voevent_position3d->unit){
        #ifdef DEBUG
            printf("Freeing unit attribute\n");
        #endif
        xmlFree(voevent_position3d->unit);
    }

}

/**
 * @brief      Free the VOEvent AstroCoords element
 *
 * @param      voevent_astrocoords  A pointer to a VOEvent_AstroCoords structure
 */
void free_voevent_astrocoords(struct VOEvent_AstroCoords *voevent_astrocoords){

    if(voevent_astrocoords->coord_system_id){
        #ifdef DEBUG
            printf("Freeing AstroCoords coord_system_id attribute\n");
        #endif
        xmlFree(voevent_astrocoords->coord_system_id);
    }

    if(voevent_astrocoords->Time){
        #ifdef DEBUG
            printf("Freeing Time element\n");
        #endif
        free_voevent_time(voevent_astrocoords->Time);
        free(voevent_astrocoords->Time);
    }

    if(voevent_astrocoords->Position2D){
        #ifdef DEBUG
            printf("Freeing Position2D\n");
        #endif
        free_voevent_position2d(voevent_astrocoords->Position2D);
        free(voevent_astrocoords->Position2D);
    }

    if(voevent_astrocoords->Position3D){
        #ifdef DEBUG
            printf("Freeing Position3D\n");
        #endif
        free_voevent_position3d(voevent_astrocoords->Position3D);
        free(voevent_astrocoords->Position3D);
    }

}

/**
 * @brief      Free the VOEvent ObservatoryLocation element
 *
 * @param      voevent_observatorylocation  A pointer to a VOEvent_ObservatoryLocation structure
 */
void free_voevent_observatorylocation(struct VOEvent_ObservatoryLocation *voevent_observatorylocation){

    if(voevent_observatorylocation->AstroCoordSystem){
        #ifdef DEBUG
            printf("Freeing AstroCoordSystem\n");
        #endif
        xmlFree(voevent_observatorylocation->AstroCoordSystem);
    }

    if(voevent_observatorylocation->id){
        #ifdef DEBUG
            printf("Freeing ObservatoryLocation id attribute.\n");
        #endif
        xmlFree(voevent_observatorylocation->id);
    }

    if(voevent_observatorylocation->AstroCoords){
        #ifdef DEBUG
            printf("Freeing AstroCoords\n");
        #endif
        free_voevent_astrocoords(voevent_observatorylocation->AstroCoords);
        free(voevent_observatorylocation->AstroCoords);
    }

}

/**
 * @brief      Free the VOEvent ObservationLocation element
 *
 * @param      voevent_observationlocation  A pointer to a VOEvent_ObservationLocation structure
 */
void free_voevent_observationlocation(struct VOEvent_ObservationLocation *voevent_observationlocation){

    if(voevent_observationlocation->AstroCoordSystem){
        #ifdef DEBUG
            printf("Freeing AstroCoordSystem\n");
        #endif
        xmlFree(voevent_observationlocation->AstroCoordSystem);
    }

    if(voevent_observationlocation->AstroCoords){
        #ifdef DEBUG
            printf("Freeing AstroCoords\n");
        #endif
        free_voevent_astrocoords(voevent_observationlocation->AstroCoords);
        free(voevent_observationlocation->AstroCoords);
    }

}

/**
 * @brief      Free the VOEvent ObsDataLocation element
 *
 * @param      voevent_obsdatalocation  A pointer to a VOEvent_ObsDataLocation structure
 */
void free_voevent_obsdatalocation(struct VOEvent_ObsDataLocation *voevent_obsdatalocation){

    if(voevent_obsdatalocation->ObservatoryLocation){
        #ifdef DEBUG
            printf("Freeing ObservatoryLocation\n");
        #endif
        free_voevent_observatorylocation(voevent_obsdatalocation->ObservatoryLocation);
        free(voevent_obsdatalocation->ObservatoryLocation);
    }

    if(voevent_obsdatalocation->ObservationLocation){
        #ifdef DEBUG
            printf("Freeing ObservationLocation\n");
        #endif
        free_voevent_observationlocation(voevent_obsdatalocation->ObservationLocation);
        free(voevent_obsdatalocation->ObservationLocation);
    }

}

/**
 * @brief      Free the VOEvent Description element
 *
 * @param      voevent  A pointer to a VOEvent structure
 */
void free_voevent_wherewhen(struct VOEvent *voevent){

    if((voevent->WhereWhen)->ObsDataLocation){
        #ifdef DEBUG
            printf("Freeing ObsDataLocation\n");
        #endif
        free_voevent_obsdatalocation((voevent->WhereWhen)->ObsDataLocation);
        free((voevent->WhereWhen)->ObsDataLocation);
    }

    if((voevent->WhereWhen)->Description){
        free_voevent_description((voevent->WhereWhen)->Description);
        free((voevent->WhereWhen)->Description);
    }

    if((voevent->WhereWhen)->Reference){
        free_voevent_reference((voevent->WhereWhen)->Reference);
        free((voevent->WhereWhen)->Reference);
    }

    if((voevent->WhereWhen)->id){
        #ifdef DEBUG
            printf("Freeing WhereWhen id attribute\n");
        #endif
        xmlFree((voevent->WhereWhen)->id);
    }

    free(voevent->WhereWhen);

}

/**
 * @brief      Free the VOEvent How element
 *
 * @param      voevent  A pointer to a VOEvent structure
 */
void free_voevent_how(struct VOEvent *voevent){
    if ((voevent->How)->Reference){
        int i;
        for (i = 0; i < voevent->How->nr_reference; i++){
            free_voevent_reference(&(voevent->How->Reference[i]));
        }
        free((voevent->How)->Reference);
    }

    if ((voevent->How)->Description){
        int j;
        for (j = 0; j < voevent->How->nr_description; j++){
            free_voevent_description(&(voevent->How->Description[j]));
        }
        free((voevent->How)->Description);
    }

    free(voevent->How);
}

/**
 * @brief      Free the VOEvent EventIVORN element
 *
 * @param      event_ivorn  A pointer to a VOEvent_EventIVORN structure
 */
void free_voevent_event_ivorn(struct VOEvent_EventIVORN *event_ivorn){

    if(event_ivorn->cite){
        #ifdef DEBUG
            printf("Freeing cite attribute of EventIVORN\n");
        #endif
        xmlFree(event_ivorn->cite);
    }

    if(event_ivorn->ivorn){
        #ifdef DEBUG
            printf("Freeing ivorn of EventIVORN\n");
        #endif
        xmlFree(event_ivorn->ivorn);
    }

}

/**
 * @brief      Free the VOEvent Citations element
 *
 * @param      voevent  A pointer to a VOEvent structure
 */
void free_voevent_citations(struct VOEvent *voevent){
    if((voevent->Citations)->EventIVORN){
        int j;
        for (j = 0; j < (voevent->Citations)->nr_event_ivorn; j++){
            free_voevent_event_ivorn(&((voevent->Citations)->EventIVORN[j]));
        }
        free((voevent->Citations)->EventIVORN);
    }

    if((voevent->Citations)->Description){
        free_voevent_description((voevent->Citations)->Description);
        free((voevent->Citations)->Description);
    }

    free(voevent->Citations);
}

/**
 * @brief      Free the VOEvent Name element
 *
 * @param      voevent_name  A pointer to a VOEvent_Name structure
 */
void free_voevent_name(struct VOEvent_Name *voevent_name){
    if(voevent_name->name){
        #ifdef DEBUG
            printf("Freeing name\n");
        #endif
        xmlFree(voevent_name->name);
    }
}

/**
 * @brief      Free the VOEvent Concept element
 *
 * @param      voevent_concept  A pointer to a VOEvent_Concept structure
 */
void free_voevent_concept(struct VOEvent_Concept *voevent_concept){
    if(voevent_concept->concept){
        #ifdef DEBUG
            printf("Freeing concept\n");
        #endif
        xmlFree(voevent_concept->concept);
    }
}

/**
 * @brief      Free the VOEvent Inference element
 *
 * @param      voevent_inference  A pointer to a VOEvent_Inference structure
 */
void free_voevent_inference(struct VOEvent_Inference *voevent_inference){

    if((voevent_inference)->Name){
        int j;
        for (j = 0; j < (voevent_inference)->nr_name; j++){
            free_voevent_name(&((voevent_inference)->Name[j]));
        }
        free((voevent_inference)->Name);
    }

    if((voevent_inference)->Concept){
        int j;
        for (j = 0; j < (voevent_inference)->nr_concept; j++){
            free_voevent_concept(&((voevent_inference)->Concept[j]));
        }
        free((voevent_inference)->Concept);
    }

    if((voevent_inference)->Reference){
        int j;
        for (j = 0; j < (voevent_inference)->nr_reference; j++){
            free_voevent_reference(&(voevent_inference->Reference[j]));
        }
        free((voevent_inference)->Reference);
    }

    if((voevent_inference)->Description){
        int j;
        for (j = 0; j < (voevent_inference)->nr_description; j++){
            free_voevent_description(&(voevent_inference->Description[j]));
        }
        free((voevent_inference)->Description);
    }

    if(voevent_inference->probability){
        #ifdef DEBUG
            printf("Freeing probability\n");
        #endif
        xmlFree(voevent_inference->probability);
    }

    if(voevent_inference->relation){
        #ifdef DEBUG
            printf("Freeing relation\n");
        #endif
        xmlFree(voevent_inference->relation);
    }

}

/**
 * @brief      Free the VOEvent Why element
 *
 * @param      voevent  A pointer to a VOEvent structure
 */
void free_voevent_why(struct VOEvent *voevent){

    if((voevent->Why)->Name){
        int j;
        for (j = 0; j < (voevent->Why)->nr_name; j++){
            free_voevent_name(&((voevent->Why)->Name[j]));
        }
        free((voevent->Why)->Name);
    }

    if((voevent->Why)->Concept){
        int j;
        for (j = 0; j < (voevent->Why)->nr_concept; j++){
            free_voevent_concept(&((voevent->Why)->Concept[j]));
        }
        free((voevent->Why)->Concept);
    }

    if((voevent->Why)->Inference){
        int j;
        for (j = 0; j < (voevent->Why)->nr_inference; j++){
            free_voevent_inference(&(voevent->Why->Inference[j]));
        }
        free((voevent->Why)->Inference);
    }

    if((voevent->Why)->Reference){
        int j;
        for (j = 0; j < (voevent->Why)->nr_reference; j++){
            free_voevent_reference(&(voevent->Why->Reference[j]));
        }
        free((voevent->Why)->Reference);
    }

    if((voevent->Why)->Description){
        int j;
        for (j = 0; j < (voevent->Why)->nr_description; j++){
            free_voevent_description(&(voevent->Why->Description[j]));
        }
        free((voevent->Why)->Description);
    }

    if((voevent->Why)->importance){
        xmlFree((voevent->Why)->importance);
    }

    if((voevent->Why)->expires){
        xmlFree((voevent->Why)->expires);
    }

    free(voevent->Why);
}

/**
 * @brief      Free the VOEvent
 *
 * @param      voevent  A pointer to a VOEvent structure
 */
void free_voevent(struct VOEvent *voevent){

    // free VOEvent and its members
    if(voevent->VOEvent){
        #ifdef DEBUG
            printf("Freeing root node\n");
        #endif
        free_voevent_root(voevent);
    }

    if(voevent->Who){
        #ifdef DEBUG
            printf("Freeing who node\n");
        #endif
        free_voevent_who(voevent);
    }

    if(voevent->What){
        #ifdef DEBUG
            printf("Freeing what node\n");
        #endif
        free_voevent_what(voevent);
    }

    if(voevent->WhereWhen){
        #ifdef DEBUG
            printf("Freeing wherewhen node\n");
        #endif
        free_voevent_wherewhen(voevent);
    }

    if(voevent->How){
        #ifdef DEBUG
            printf("Freeing how node\n");
        #endif
        free_voevent_how(voevent);
    }

    if(voevent->Citations){
        #ifdef DEBUG
            printf("Freeing citations node\n");
        #endif
        free_voevent_citations(voevent);
    }

    if(voevent->Why){
        #ifdef DEBUG
            printf("Freeing why node\n");
        #endif
        free_voevent_why(voevent);
    }

    if(voevent->Description){
        #ifdef DEBUG
            printf("Freeing description node\n");
        #endif
        free_voevent_description(voevent->Description);
        free(voevent->Description);
    }

    if(voevent->Reference){
        #ifdef DEBUG
            printf("Freeing reference node\n");
        #endif
        free_voevent_reference(voevent->Reference);
        free(voevent->Reference);
    }

    if(voevent->Doc){
        #ifdef DEBUG
            printf("Freeing document tree pointer\n");
        #endif
        xmlFreeDoc(voevent->Doc);
    }
    //xmlCleanupParser(); //safe to call: if parser is not initialized, it will do nothing

    //free(voevent); NOT NEEDED!
}

/**
 * @brief      Free the Transport meta element
 *
 * @param      transport  A pointer to a Transport structure
 */
void free_transport_meta(struct Transport *transport){

    if((transport->Meta)->result){
        #ifdef DEBUG
            printf("Freeing result\n");
        #endif
        xmlFree((transport->Meta)->result);
    }

    if((transport->Meta)->meta_param){
        int j;
        for (j = 0; j < transport->Meta->nr_params; j++){
            free_voevent_param(&(transport->Meta->meta_param[j]));
        }
        free((transport->Meta)->meta_param);
    }

    free(transport->Meta);

}

/**
 * @brief      Free the Transport root element
 *
 * @param      transport  A pointer to a Transport structure
 */
void free_transport_root(struct Transport *transport){

    if((transport->Transport)->version){
        #ifdef DEBUG
            printf("Freeing version\n");
        #endif
        xmlFree((transport->Transport)->version);
    }

    if((transport->Transport)->role){
        #ifdef DEBUG
            printf("Freeing role\n");
        #endif
        xmlFree((transport->Transport)->role);
    }

    free(transport->Transport);
}

/**
 * @brief      Free the Transport
 *
 * @param      transport  A pointer to a Transport structure
 */
void free_transport(struct Transport *transport){

    // free Transport and its members
    if(transport->Transport){
        #ifdef DEBUG
            printf("Freeing root node\n");
        #endif
        free_transport_root(transport);
    }

    if(transport->Origin){
        #ifdef DEBUG
            printf("Freeing Origin\n");
        #endif
        xmlFree(transport->Origin);
    }

    if(transport->TimeStamp){
        #ifdef DEBUG
            printf("Freeing TimeStamp\n");
        #endif
        xmlFree(transport->TimeStamp);
    }

    if(transport->Response){
        #ifdef DEBUG
            printf("Freeing Response\n");
        #endif
        xmlFree(transport->Response);
    }

    if(transport->Meta){
        #ifdef DEBUG
            printf("Freeing Meta\n");
        #endif
        free_transport_meta(transport);
    }

    if(transport->Doc){
        #ifdef DEBUG
            printf("Freeing document tree pointer\n");
        #endif
        xmlFreeDoc(transport->Doc);
    }
    //xmlCleanupParser(); //safe to call: if parser is not initialized, it will do nothing

    //free(voevent); NOT NEEDED!
}
