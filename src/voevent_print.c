/**
 * @file voevent_print.c
 * @brief Printing function for VOEvents.
 *
 */

#include <stdio.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlschemastypes.h>
#include <libxml/xpath.h>
#include "voevent.h"


/**
 * @brief      Print content of a Description element
 *
 * @param      description  A VOEvent_Description structure
 */
void print_voevent_description(struct VOEvent_Description *description){
    if(description->description){
        printf("Description : %s\n", (char *)description->description);
    }
}

/**
 * @brief      Print content of a Reference element
 *
 * @param      reference  A VOEvent_Reference structure
 */
void print_voevent_reference(struct VOEvent_Reference *reference){
    if(reference->uri){
        printf("Reference uri      : %s\n", (char *)reference->uri);
    }
    if(reference->type){
        printf("Reference type     : %s\n", (char *)reference->type);
    }
    if(reference->mimetype){
        printf("Reference mimetype : %s\n", (char *)reference->mimetype);
    }
    if(reference->meaning){
        printf("Reference meaning  : %s\n", (char *)reference->meaning);
    }
}

/**
 * @brief      Print content of an Author element
 *
 * @param      voevent  A VOEvent structure
 */
void print_author(struct VOEvent *voevent){
    
    struct VOEvent_Author *voevent_author = get_author(voevent);

    if(voevent_author->title){
        printf("VOEvent Author title         : %s\n", (char *)voevent_author->title);
    }

    if(voevent_author->shortName){
        printf("VOEvent Author short name    : %s\n", (char *)voevent_author->shortName);
    }

    if(voevent_author->logoURL){
        printf("VOEvent Author logo URL      : %s\n", (char *)voevent_author->logoURL);
    }

    if(voevent_author->contactName){
        printf("VOEvent Author contact name  : %s\n", (char *)voevent_author->contactName);
    }

    if(voevent_author->contactEmail){
        printf("VOEvent Author contact email : %s\n", (char *)voevent_author->contactEmail);
    }

    if(voevent_author->contactPhone){
        printf("VOEvent Author contact phone : %s\n", (char *)voevent_author->contactPhone);
    }

    if(voevent_author->contributor){
        printf("VOEvent Author contributor   : %s\n", (char *)voevent_author->contributor);
    }

}

/**
 * @brief      Print content of the VOEvent root element
 *
 * @param      voevent  A VOEvent structure
 */
void print_voevent_header(struct VOEvent *voevent){

    printf("================== VOEVENT HEADER ==================\n");
    printf("VOEvent IVORN   : %s\n", get_ivorn(voevent));
    printf("VOEvent role    : %s\n", get_role(voevent));
    printf("VOEvent version : %s\n", get_version(voevent));
    printf("================ END VOEVENT HEADER ================\n");
    printf("\n");
}

/**
 * @brief      Print content of a Who element
 *
 * @param      voevent  A VOEvent structure
 */
void print_voevent_who(struct VOEvent *voevent){
    if(voevent->Who){

        char *author_ivorn = get_author_ivorn(voevent);
        struct VOEvent_Author *author = get_author(voevent);
        char *voevent_date = get_date(voevent);

        if (author_ivorn != NULL || author != NULL || voevent_date != NULL || (voevent->Who)->Description != NULL){
            printf("=================  VOEVENT WHO  ====================\n");

            if(author_ivorn){
                printf("Author IVORN : %s\n", author_ivorn);
            }

            if(author){
                print_author(voevent);
            }

            if(voevent_date){
                printf("VOEvent Date : %s\n", voevent_date);
            }

            if(((voevent->Who)->Description)){
                printf("VOEvent Who Description : %s\n", (char *)((voevent->Who)->Description)->description);
            }

            printf("===============  END VOEVENT WHO  ==================\n");
            printf("\n");
        }
    }
}

/**
 * @brief      Print content of a EventIVORN element
 *
 * @param      event_ivorn  A VOEvent_EventIVORN structure
 */
void print_event_ivorn(struct VOEvent_EventIVORN *event_ivorn){

    printf("Event IVORN: %s (cite: %s)\n", (char *)event_ivorn->ivorn, (char *)event_ivorn->cite);

}

/**
 * @brief      Print content of a Param element
 *
 * @param      param  A VOEvent_Param structure
 */
void print_voevent_param(struct VOEvent_Param *param){

    if(param->name && param->value){
        if(param->unit){
            printf("%s: %s %s\n", (char *)param->name, (char *)param->value, (char *)param->unit);
        }
        else{
            printf("%s: %s\n", (char *)param->name, (char *)param->value);
        }
    }

    //if(param->ucd){
    //   printf("Parameter ucd      : %s\n", (char *)param->ucd);
    //}

    if(param->dataType){
        printf("Parameter dataType : %s\n", (char *)param->dataType);
    }

    if(param->utype){
        printf("Parameter utype    : %s\n", (char *)param->utype);
    }

    if(param->Value){
        printf("Parameter Value    : %s\n", (char *)param->Value);
    }

    if(param->Reference){
        int i = 0;
        for (i = 0; i < param->nr_reference; i++){
            printf("Reference no. %d\n", i+1);
            print_voevent_reference(&(param->Reference[i]));
        }
    }

    if(param->Description){
        int j;
        for (j = 0; j < param->nr_description; j++){
            printf("Description no. %d\n", j+1);
            print_voevent_description(&(param->Description[j]));
        }
    }

}

/**
 * @brief      Print content of a Group element
 *
 * @param      group  A VOEvent_Group structure
 */
void print_voevent_group(struct VOEvent_Group *group){

    if(group->name){
        printf("Group name : %s\n", (char *)group->name);
    }

    if(group->type){
        printf("Group type : %s\n", (char *)group->type);
    }   

    if(group->Reference){
        int i = 0;
        for (i = 0; i < group->nr_reference; i++){
            printf("Reference no. %d\n", i+1);
            print_voevent_reference(&(group->Reference[i]));
        }
        printf("\n");
    }

    if(group->Description){
        int j;
        for (j = 0; j < group->nr_description; j++){
            printf("Description no. %d\n", j+1);
            print_voevent_description(&(group->Description[j]));
        }
        printf("\n");
    }

    if(group->Param){
        int j;
        for (j = 0; j < group->nr_params; j++){
            print_voevent_param(&(group->Param[j]));
        }
        printf("\n");
    }

}

/**
 * @brief      Print content of a Field element
 *
 * @param      field  A VOEvent_Field structure
 */
void print_voevent_field(struct VOEvent_Field *field){

    if(field->name){
        if(field->unit){
            printf("%-10s ", (char *)field->name);
        }
        else{
            printf("%-10s ", (char *)field->name);
        }
    }

    //if(field->ucd){
    //   printf("Field ucd      : %s\n", (char *)field->ucd);
    //}

    if(field->dataType){
        printf("Field dataType : %s\n", (char *)field->dataType);
    }
    /*
    if(field->utype){
        printf("Field utype    : %s\n", (char *)field->utype);
    }
    */
    if(field->Reference){
        int i = 0;
        for (i = 0; i < field->nr_reference; i++){
            printf("Reference no. %d\n", i+1);
            print_voevent_reference(&(field->Reference[i]));
        }
    }
    /*
    if(field->Description){
        int j;
        for (j = 0; j < field->nr_description; j++){
            printf("Description no. %d\n", j+1);
            print_voevent_description(&(field->Description[j]));
        }
    }
    */
}

/**
 * @brief      Print content of a Data element
 *
 * @param      data        A VOEvent_Data structure
 * @param[in]  nr_rows     Number of rows
 * @param[in]  nr_columns  Number of columns
 */
void print_voevent_data(struct VOEvent_Data *data, int nr_rows, int nr_columns){
    
    int i,j;
    for (i = 0; i < nr_rows; ++i)
    {
        for (j = 0; j < nr_columns; ++j)
        {
            printf("%-10s ", data[j+i*nr_columns].data);
        }
        printf("\n");
    }
}

/**
 * @brief      Print content of a Table element
 *
 * @param      table  A VOEvent_Table structure
 */
void print_voevent_table(struct VOEvent_Table *table){

    if(table->name){
        printf("Group name : %s\n", (char *)table->name);
    }

    if(table->type){
        printf("Group type : %s\n", (char *)table->type);
    }   

    if(table->Reference){
        int i = 0;
        for (i = 0; i < table->nr_reference; i++){
            printf("Reference no. %d\n", i+1);
            print_voevent_reference(&(table->Reference[i]));
        }
        printf("\n");
    }

    if(table->Param){
        int j;
        for (j = 0; j < table->nr_params; j++){
            print_voevent_param(&(table->Param[j]));
        }
        printf("\n");
    }

    if(table->Description){
        int j;
        for (j = 0; j < table->nr_description; j++){
            printf("Description no. %d\n", j+1);
            print_voevent_description(&(table->Description[j]));
        }
        printf("\n");
    }

    if(table->Field){
        int j;
        for (j = 0; j < table->nr_columns; j++){
            print_voevent_field(&(table->Field[j]));
        }
        printf("\n");
    }

    if(table->Data){
        print_voevent_data(table->Data, table->nr_rows, table->nr_columns);
        printf("\n");
    }

}

/**
 * @brief      Print content of a What element
 *
 * @param      voevent  A VOEvent structure
 */
void print_voevent_what(struct VOEvent *voevent){
    if(voevent->What){
        if(voevent->What->nr_reference || voevent->What->nr_description ||
            voevent->What->nr_params || voevent->What->nr_groups ||
            voevent->What->nr_tables){

            printf("=================  VOEVENT WHAT  ===================\n");

            if((voevent->What)->Reference){
                int i = 0;
                for (i = 0; i < voevent->What->nr_reference; i++){
                    printf("Reference no. %d\n", i+1);
                    print_voevent_reference(&((voevent->What)->Reference[i]));
                }
            }

            if(((voevent->What)->Description)){
                int j;
                for (j = 0; j < voevent->What->nr_description; j++){
                    printf("Description no. %d\n", j+1);
                    print_voevent_description(&((voevent->What)->Description[j]));
                }
            }

            if(((voevent->What)->Param)){
                int j;
                for (j = 0; j < voevent->What->nr_params; j++){
                    print_voevent_param(&((voevent->What)->Param[j]));
                }
            }

            if(((voevent->What)->Group)){
                int j;
                for (j = 0; j < voevent->What->nr_groups; j++){
                    print_voevent_group(&((voevent->What)->Group[j]));
                }
            }

            if(((voevent->What)->Table)){
                int j;
                for (j = 0; j < voevent->What->nr_tables; j++){
                    print_voevent_table(&((voevent->What)->Table[j]));
                }
            }

            printf("===============  END VOEVENT WHAT  =================\n");
            printf("\n");
        }
    }
}

/**
 * @brief      Print content of a Position2D element
 *
 * @param      voevent_position2d  A VOEvent_Position2D structure
 */
void print_voevent_position2d(struct VOEvent_Position2D *voevent_position2d){

    if(voevent_position2d->Name1){
        printf("Coordinate 1 name: %s\n", (char *)voevent_position2d->Name1);
    }

    if(voevent_position2d->Name2){
        printf("Coordinate 2 name: %s\n", (char *)voevent_position2d->Name2);
    }

    if(voevent_position2d->C1){
        printf("Coordinate 1 value: %s\n", (char *)voevent_position2d->C1);
    }

    if(voevent_position2d->C2){
        printf("Coordinate 2 value: %s\n", (char *)voevent_position2d->C2);
    }

    if(voevent_position2d->ErrorRadius){
        printf("Coordinate error: %s\n", (char *)voevent_position2d->ErrorRadius);
    }

    if(voevent_position2d->unit){
        printf("Coordinates unit: %s\n", voevent_position2d->unit);
    }

}

/**
 * @brief      Print content of a Position3D element
 *
 * @param      voevent_position3d  A VOEvent_Position3D structure
 */
void print_voevent_position3d(struct VOEvent_Position3D *voevent_position3d){

    if(voevent_position3d->Name1){
        printf("Coordinate 1 name: %s\n", (char *)voevent_position3d->Name1);
    }

    if(voevent_position3d->Name2){
        printf("Coordinate 2 name: %s\n", (char *)voevent_position3d->Name2);
    }

    if(voevent_position3d->Name3){
        printf("Coordinate 3 name: %s\n", (char *)voevent_position3d->Name3);
    }

    if(voevent_position3d->C1){
        printf("Coordinate 1 value: %s\n", (char *)voevent_position3d->C1);
    }

    if(voevent_position3d->C2){
        printf("Coordinate 2 value: %s\n", (char *)voevent_position3d->C2);
    }

    if(voevent_position3d->C3){
        printf("Coordinate 3 value: %s\n", (char *)voevent_position3d->C3);
    }

    if(voevent_position3d->unit){
        printf("Coordinates unit: %s\n", voevent_position3d->unit);
    }

}

/**
 * @brief      Print content of a TimeInstant element
 *
 * @param      voevent_timeinstant  A VOEvent_TimeInstant structure
 */
void print_voevent_timeinstant(struct VOEvent_TimeInstant *voevent_timeinstant){

    if(voevent_timeinstant->ISOTime){
        printf("ISO Time: %s\n", (char *)voevent_timeinstant->ISOTime);
    }

    if(voevent_timeinstant->TimeOffset){
        printf("Time Offset: %s\n", (char *)voevent_timeinstant->TimeOffset);
    }

    if(voevent_timeinstant->TimeScale){
        printf("Time scale: %s\n", (char *)voevent_timeinstant->TimeScale);
    }

}

/**
 * @brief      Print content of a Time element
 *
 * @param      voevent_time  A VOEvent_Time structure
 */
void print_voevent_time(struct VOEvent_Time *voevent_time){

    if(voevent_time->Error){
        printf("Time error: %s\n", (char *)voevent_time->Error);
    }

    if(voevent_time->TimeInstant){
        print_voevent_timeinstant(voevent_time->TimeInstant);
    }

}

/**
 * @brief      Print content of a AstroCoords element
 *
 * @param      voevent_astrocoords  A VOEvent_AstroCoords structure
 */
void print_voevent_astrocoords(struct VOEvent_AstroCoords *voevent_astrocoords){

    if(voevent_astrocoords->coord_system_id){
        printf("Astronomical coordinates system: %s\n", (char *)voevent_astrocoords->coord_system_id);
    }

    if(voevent_astrocoords->Time){
        print_voevent_time(voevent_astrocoords->Time);
    }

    if(voevent_astrocoords->Position2D){
        print_voevent_position2d(voevent_astrocoords->Position2D);
    }

    if(voevent_astrocoords->Position3D){
        print_voevent_position3d(voevent_astrocoords->Position3D);
    }

    return;
}

/**
 * @brief      Print content of a ObservationLocation element
 *
 * @param      voevent_observationlocation  A VOEvent_ObservationLocation structure
 */
void print_voevent_observationlocation(struct VOEvent_ObservationLocation *voevent_observationlocation){

    if(voevent_observationlocation->AstroCoordSystem){
        printf("Astronomical coordinates system: %s\n", (char *)voevent_observationlocation->AstroCoordSystem);
    }

    if(voevent_observationlocation->AstroCoords){
        print_voevent_astrocoords(voevent_observationlocation->AstroCoords);
    }

    return;
}

/**
 * @brief      Print content of a ObservatoryLocation element
 *
 * @param      voevent_observatorylocation  A VOEvent_ObservatoryLocation structure
 */
void print_voevent_observatorylocation(struct VOEvent_ObservatoryLocation *voevent_observatorylocation){

    if(voevent_observatorylocation->AstroCoordSystem){
        printf("Astronomical coordinates system is: %s\n", (char *)voevent_observatorylocation->AstroCoordSystem);
    }

    if(voevent_observatorylocation->id){
        printf("Observatory location ID: %s\n", (char *)voevent_observatorylocation->id);
    }

    if(voevent_observatorylocation->AstroCoords){
        print_voevent_astrocoords(voevent_observatorylocation->AstroCoords);
    }

    return;
}

/**
 * @brief      Print content of a ObsDataLocation element
 *
 * @param      voevent_obsdatalocation  A VOEvent_ObsDataLocation structure
 */
void print_voevent_obsdatalocation(struct VOEvent_ObsDataLocation *voevent_obsdatalocation){

    if(voevent_obsdatalocation->ObservatoryLocation){
        print_voevent_observatorylocation(voevent_obsdatalocation->ObservatoryLocation);
    }

    if(voevent_obsdatalocation->ObservationLocation){
        print_voevent_observationlocation(voevent_obsdatalocation->ObservationLocation);
    }
  
}

/**
 * @brief      Print content of a WhereWhen element
 *
 * @param      voevent  A VOEvent structure
 */
void print_voevent_wherewhen(struct VOEvent *voevent){
    if(voevent->WhereWhen){
        if((voevent->WhereWhen)->ObsDataLocation){
            printf("===============  VOEVENT WHEREWHEN  ================\n");

            print_voevent_obsdatalocation((voevent->WhereWhen)->ObsDataLocation);

            if((voevent->WhereWhen)->Description){
                print_voevent_description((voevent->WhereWhen)->Description);
            }

            if((voevent->WhereWhen)->Reference){
                print_voevent_reference((voevent->WhereWhen)->Reference);
            }

            if((voevent->WhereWhen)->id){
                printf("WhereWhen id: %s\n", (voevent->WhereWhen)->id);
            }

            printf("=============  END VOEVENT WHEREWHEN  ==============\n");
            printf("\n");
        }
    }
}

/**
 * @brief      Print content of a How element
 *
 * @param      voevent  A VOEvent structure
 */
void print_voevent_how(struct VOEvent *voevent){
    if(voevent->How){
        if (voevent->How->nr_reference || voevent->How->nr_description){
            printf("==================  VOEVENT HOW  ===================\n");

            if((voevent->How)->Reference){
                int i = 0;
                for (i = 0; i < voevent->How->nr_reference; i++){
                    printf("Reference no %d\n", i+1);
                    print_voevent_reference(&((voevent->How)->Reference[i]));
                }
            }

            if(((voevent->How)->Description)){
                int j;
                for (j = 0; j < voevent->How->nr_description; j++){
                    printf("Description no. %d\n", j+1);
                    print_voevent_description(&((voevent->How)->Description[j]));
                }
            }

            printf("================= END VOEVENT HOW  =================\n");
            printf("\n");
        }
    }
}

/**
 * @brief      Print content of a Citations element
 *
 * @param      voevent  A VOEvent structure
 */
void print_voevent_citations(struct VOEvent *voevent){
    if(voevent->Citations){
        if((voevent->Citations)->EventIVORN){
            printf("==============  VOEVENT CITATIONS  =================\n");
            int j;
            for (j = 0; j < (voevent->Citations)->nr_event_ivorn; j++){
                print_event_ivorn(&((voevent->Citations)->EventIVORN[j]));
            }
            if(((voevent->Citations)->Description)){
                printf("VOEvent Citations Description : %s\n", (char *)((voevent->Citations)->Description)->description);
            }
            printf("============== END VOEVENT CITATIONS  ==============\n");
            printf("\n");
        }
    }
}

/**
 * @brief      Print content of a Name element
 *
 * @param      voevent_name  A VOEvent_Name structure
 */
void print_voevent_name(struct VOEvent_Name *voevent_name){
    if(voevent_name->name){
        printf("Name : %s\n", (char *)voevent_name->name);
    }
}

/**
 * @brief      Print content of a Concept element
 *
 * @param      voevent_concept  A VOEvent_Concept structure
 */
void print_voevent_concept(struct VOEvent_Concept *voevent_concept){
    if(voevent_concept->concept){
        printf("Concept : %s\n", (char *)voevent_concept->concept);
    }
}

/**
 * @brief      Print content of a Inference element
 *
 * @param      voevent_inference  A VOEvent_Inference structure
 */
void print_voevent_inference(struct VOEvent_Inference *voevent_inference){

    if((voevent_inference)->Name){
        int j;
        for (j = 0; j < (voevent_inference)->nr_name; j++){
            print_voevent_name(&((voevent_inference)->Name[j]));
        }
    }

    if((voevent_inference)->Concept){
        int j;
        for (j = 0; j < (voevent_inference)->nr_concept; j++){
            print_voevent_concept(&((voevent_inference)->Concept[j]));
        }
    }

    if((voevent_inference)->Reference){
        int j;
        for (j = 0; j < (voevent_inference)->nr_reference; j++){
            print_voevent_reference(&(voevent_inference->Reference[j]));
        }
    }

    if((voevent_inference)->Description){
        int j;
        for (j = 0; j < (voevent_inference)->nr_description; j++){
            print_voevent_description(&(voevent_inference->Description[j]));
        }
    }

    if(voevent_inference->probability){
        printf("Probability: %s\n", (char *)voevent_inference->probability);
    }

    if(voevent_inference->relation){
        printf("Relation: %s\n", (char *)voevent_inference->relation);
    }

}

/**
 * @brief      Print content of a Why element
 *
 * @param      voevent  A VOEvent structure
 */
void print_voevent_why(struct VOEvent *voevent){
    if(voevent->Why){
        if((voevent->Why)->importance || (voevent->Why)->expires){
            printf("==================  VOEVENT WHY  ===================\n");

            if((voevent->Why)->Name){
                int j;
                for (j = 0; j < (voevent->Why)->nr_name; j++){
                    print_voevent_name(&((voevent->Why)->Name[j]));
                }
            }

            if((voevent->Why)->Concept){
                int j;
                for (j = 0; j < (voevent->Why)->nr_concept; j++){
                    print_voevent_concept(&((voevent->Why)->Concept[j]));
                }
            }

            if((voevent->Why)->Inference){
                int j;
                for (j = 0; j < (voevent->Why)->nr_inference; j++){
                    print_voevent_inference(&(voevent->Why->Inference[j]));
                }
            }

            if((voevent->Why)->Reference){
                int j;
                for (j = 0; j < (voevent->Why)->nr_reference; j++){
                    print_voevent_reference(&(voevent->Why->Reference[j]));
                }
            }

            if((voevent->Why)->Description){
                int j;
                for (j = 0; j < (voevent->Why)->nr_description; j++){
                    print_voevent_description(&(voevent->Why->Description[j]));
                }
            }

            if((voevent->Why)->importance){
                printf("Importance: %s\n", (voevent->Why)->importance);
            }

            if((voevent->Why)->expires){
                printf("Expires: %s\n", (voevent->Why)->expires);
            }

            printf("================= END VOEVENT WHY  =================\n");
            printf("\n");
        }
    }
}

/**
 * @brief      Print content of a VOEvent
 *
 * @param      voevent  A VOEvent structure
 */
void print_voevent(struct VOEvent *voevent){
    
    print_voevent_header(voevent);
    print_voevent_who(voevent);
    print_voevent_what(voevent);
    print_voevent_wherewhen(voevent);
    print_voevent_citations(voevent);
    print_voevent_how(voevent);

    if (voevent->Description)
    {
        if (voevent->Description->description){
            if (!strcmp((char *)voevent->Description->description, "")){
                printf("==============  VOEVENT DESCRIPTION  ===============\n");
                print_voevent_description(voevent->Description);
                printf("============= END VOEVENT DESCRIPTION  =============\n");
                printf("\n");
            }
        }
    }

    print_voevent_why(voevent);

    if (voevent->Reference)
    {
        if (voevent->Reference->uri){
            printf("===============  VOEVENT REFERENCE  ================\n");
            print_voevent_reference(voevent->Reference);
            printf("============== END VOEVENT REFERENCE  ==============\n");
            printf("\n");
        }
    }

}
