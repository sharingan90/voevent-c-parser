/**
 * @file voevent_utils_GCN.c
 * @brief Utility functions specific for VOEvents sent by the GCN.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "voevent.h"

/**
 * @brief      Get packet type (GCN numbering, see https://gcn.gsfc.nasa.gov/sock_pkt_def_doc.html).
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Packet type number value if found, -1 otherwise.
 */
int get_packet_type(struct VOEvent *voevent){
    struct VOEvent_Param *packet_type = get_param_by_name(voevent, "Packet_Type");
    if(packet_type){
        return atoi((char *)packet_type->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get packet serial number.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Packet serial number value if found, -1 otherwise.
 */
int get_packet_serial_number(struct VOEvent *voevent){
    struct VOEvent_Param *packet_ser_num = get_param_by_name(voevent, "Pkt_Ser_Num");
    if(packet_ser_num){
        return atoi((char *)packet_ser_num->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get packet sequential number.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Packet sequential number value if found, -1 otherwise.
 */
int get_packet_sequence_number(struct VOEvent *voevent){
    struct VOEvent_Param *packet_seq_num = get_param_by_name(voevent, "Sequence_Num");
    if(packet_seq_num){
        return atoi((char *)packet_seq_num->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get event SOD.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     SOD value if found, -1 otherwise.
 */
double get_event_sod(struct VOEvent *voevent){
    struct VOEvent_Param *event_sod = get_param_by_name(voevent, "Burst_SOD");
    if(event_sod){
        return atof((char *)event_sod->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get event SOD from TimeInstant element.
 *
 * @param      voevent       A VOEvent structure
 * @param      year          Year
 * @param      mon           Month
 * @param      day           Day
 * @param      hour          Hour
 * @param      min           Minutes
 * @param      sec           Seconds
 *
 * @return     SOD value if found, -1 otherwise.
 */
int get_generic_event_time(struct VOEvent *voevent, int *year, int *mon, int *day, int *hour, int *min, double *sec){
    if (get_time_instant(voevent))
    {
        struct VOEvent_TimeInstant *time_instant = get_time_instant(voevent);
        //printf("%s\n", pkt_time);
        char str[10];
        int n;
        n = sscanf((char *)time_instant->ISOTime, "%4d-%2d-%2d %1s %2d:%2d:%lf", year, mon, day, str, hour, min, sec);
        if(n == 7){
            return 1;
        }
        else{
            fprintf(stderr, "Packet time not scanned properly.\n");
            return 0;
        }
    }

    return 0;
}

/**
 * @brief      Get event TJD.
 *
 * @param      voevent       A VOEvent structure.
 *
 * @return     TJD value if found, -1 otherwise.
 */
double get_event_tjd(struct VOEvent *voevent){
    struct VOEvent_Param *event_tjd = get_param_by_name(voevent, "Burst_TJD");
    if(event_tjd){
        return atof((char *)event_tjd->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get packet SOD.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Packet SOD value if found, -1 otherwise.
 */
double get_packet_sod(struct VOEvent *voevent){
    struct tm packet_tm = {0};
    get_packet_time(voevent, &packet_tm);
    double packet_sod = (double)((packet_tm.tm_hour*3600)+(packet_tm.tm_min*60)+packet_tm.tm_sec);
    return packet_sod;
}

/**
 * @brief      Get RA value.
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     RA value if found, -1 otherwise.
 */
double get_ra(struct VOEvent *voevent){

    struct VOEvent_Position2D *position2d = get_position2d(voevent);
    if(position2d){
        if(position2d->Name1){
            if (!strcmp((char *)position2d->Name1, "RA")){
                return atof((char *)position2d->C1);
            }
        }
    }

    return -1;
}

/**
 * @brief      Get Declination value.
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     Declination value if fund, -1 otherwise.
 */
double get_dec(struct VOEvent *voevent){

    struct VOEvent_Position2D *position2d = get_position2d(voevent);
    if(position2d){
        if(position2d->Name1){
            if (!strcmp((char *)position2d->Name2, "Dec")){
                return atof((char *)position2d->C2);
            }
        }
    }

    return -1;
}

/**
 * @brief      Get Coordinate Error value.
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     Coordinate Error value if found, -1 otherwise.
 */
double get_coord_error(struct VOEvent *voevent){

    struct VOEvent_Position2D *position2d = get_position2d(voevent);
    if(position2d){
        if(position2d->ErrorRadius){
            return atof((char *)position2d->ErrorRadius);
        }
    }

    return -1;
}

/**
 * @brief      Get trigger number value.
 *
 * @param      voevent  A VOEvent structure
 * *
 * @return     Trigger number value if found, -1 otherwise.
 */
long long int get_trigger_number(struct VOEvent *voevent){
    struct VOEvent_Param *trigger_number = get_param_by_name(voevent, "TrigID");
    if(trigger_number){
        return strtoll((char *)trigger_number->value, NULL, 10);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get number of events.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Number of events if found, -1 otherwise.
 */
int get_event_counts(struct VOEvent *voevent){
    struct VOEvent_Param *event_counts = get_param_by_name(voevent, "Burst_Inten");
    if(event_counts){
        return atoi((char *)event_counts->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get data significance.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Data significance if found, -1 otherwise.
 */
double get_data_significance(struct VOEvent *voevent){
    struct VOEvent_Param *data_significance = get_param_by_name(voevent, "Data_Signif");
    if(data_significance){
        return atof((char *)data_significance->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get trigger significance.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Trigger significance if found, -1 otherwise.
 */
double get_trigger_significance(struct VOEvent *voevent){
    struct VOEvent_Param *trigger_significance = get_param_by_name(voevent, "Trig_Signif");
    if(trigger_significance){
        return atof((char *)trigger_significance->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get event significance.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Event significance if found, -1 otherwise.
 */
double get_event_significance(struct VOEvent *voevent){
    struct VOEvent_Param *event_significance = get_param_by_name(voevent, "Burst_Signif");
    if(event_significance){
        return atof((char *)event_significance->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get Phi angle value.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Phi angle value if found, -1 otherwise.
 */
double get_phi(struct VOEvent *voevent){
    struct VOEvent_Param *phi = get_param_by_name(voevent, "Phi");
    if(phi){
        return atof((char *)phi->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get Theta angle value.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Theta angle value if found, -1 otherwise.
 */
double get_theta(struct VOEvent *voevent){
    struct VOEvent_Param *theta = get_param_by_name(voevent, "Theta");
    if(theta){
        return atof((char *)theta->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get spacecraft longitude value.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Spacecraft longitude value if found, -1 otherwise.
 */
double get_spacecraft_longitude(struct VOEvent *voevent){
    struct VOEvent_Param *sc_long = get_param_by_name(voevent, "SC_Long");
    if(sc_long){
        return atof((char *)sc_long->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get spacecraft latitude value.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Spacecraft latitude value if found, -1 otherwise.
 */
double get_spacecraft_latitude(struct VOEvent *voevent){
    struct VOEvent_Param *sc_lat = get_param_by_name(voevent, "SC_Lat");
    if(sc_lat){
        return atof((char *)sc_lat->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get trigger timescale.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Trigger timescale value if found, -1 otherwise.
 */
double get_trigger_timescale(struct VOEvent *voevent){
    struct VOEvent_Param *trig_tscale = get_param_by_name(voevent, "Trig_Timescale");
    if(trig_tscale){
        return atof((char *)trig_tscale->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get trigger duration.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Trigger duration value if found, -1 otherwise.
 */
double get_trigger_duration(struct VOEvent *voevent){
    struct VOEvent_Param *trig_dur = get_param_by_name(voevent, "Trig_Dur");
    if(trig_dur){
        return atof((char *)trig_dur->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get data timescale.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Data timescale value if found, -1 otherwise.
 */
double get_data_timescale(struct VOEvent *voevent){
    struct VOEvent_Param *data_tscale = get_param_by_name(voevent, "Data_Timescale");
    if(data_tscale){
        return atof((char *)data_tscale->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get data integration time.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Data integration time value if found, -1 otherwise.
 */
double get_data_integration(struct VOEvent *voevent){
    struct VOEvent_Param *data_integration = get_param_by_name(voevent, "Data_Integ");
    if(data_integration){
        return atof((char *)data_integration->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get integration time.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Integration time value if found, -1 otherwise.
 */
double get_integration_time(struct VOEvent *voevent){
    struct VOEvent_Param *data_integration = get_param_by_name(voevent, "Integ_Time");
    if(data_integration){
        return atof((char *)data_integration->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get algorithm number.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Algorithm number value if found, -1 otherwise.
 */
int get_algorithm_number(struct VOEvent *voevent){
    struct VOEvent_Param *algorithm = get_param_by_name(voevent, "Algorithm");
    if(algorithm){
        return atoi((char *)algorithm->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get most likely source index.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Most likely source index if found, -1 otherwise.
 */
int get_most_likely_index(struct VOEvent *voevent){
    struct VOEvent_Param *most_likely_index = get_param_by_name(voevent, "Most_Likely_Index");
    if(most_likely_index){
        return atoi((char *)most_likely_index->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get most likely source probability.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Most likely source probability if found, -1 otherwise.
 */
int get_most_likely_probability(struct VOEvent *voevent){
    struct VOEvent_Param *most_likely_probability = get_param_by_name(voevent, "Most_Likely_Prob");
    if(most_likely_probability){
        return atoi((char *)most_likely_probability->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get second most likely source index.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Second most likely source index if found, -1 otherwise.
 */
int get_second_most_likely_index(struct VOEvent *voevent){
    struct VOEvent_Param *second_most_likely_index = get_param_by_name(voevent, "Sec_Most_Likely_Index");
    if(second_most_likely_index){
        return atoi((char *)second_most_likely_index->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get second most likely source probability.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Second most likely source probability if found, -1 otherwise.
 */
int get_second_most_likely_probability(struct VOEvent *voevent){
    struct VOEvent_Param *second_most_likely_probability = get_param_by_name(voevent, "Sec_Most_Likely_Prob");
    if(second_most_likely_probability){
        return atoi((char *)second_most_likely_probability->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get hardness ratio.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Hardness ratio value if found, -1 otherwise.
 */
double get_hardness_ratio(struct VOEvent *voevent){
    struct VOEvent_Param *hardness_ratio = get_param_by_name(voevent, "Hardness_Ratio");
    if(hardness_ratio){
        return atof((char *)hardness_ratio->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get trigger ID.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Trigger ID if found, -1 otherwise.
 */
long int get_trigger_id(struct VOEvent *voevent){
    struct VOEvent_Param *trigger_id = get_param_by_name(voevent, "Trigger_ID");
    if(trigger_id){
        return strtol((char *)trigger_id->value, NULL, 16);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get trigger index.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Trigger index if found, -1 otherwise.
 */
int get_trigger_index(struct VOEvent *voevent){
    struct VOEvent_Param *trigger_index = get_param_by_name(voevent, "Trig_Index");
    if(trigger_index){
        return atoi((char *)trigger_index->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get MISC ID.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     MISC ID if found, -1 otherwise.
 */
long int get_misc(struct VOEvent *voevent){
    struct VOEvent_Param *misc = get_param_by_name(voevent, "Misc_flags");
    if(misc){
        return strtol((char *)misc->value, NULL, 16);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get DETS ID.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     DETS ID if found, -1 otherwise.
 */
long int get_dets(struct VOEvent *voevent){
    struct VOEvent_Param *dets = get_param_by_name(voevent, "Dets");
    if(dets){
        return strtol((char *)dets->value, NULL, 16);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get Lightcurve URL.
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     Lightcurve URL, NULL otherwise.
 */
char *get_lightcurve_url(struct VOEvent * voevent){
    struct VOEvent_Param *lc_url = get_param_by_name(voevent, "LightCurve_URL");
    if(lc_url){
        return (char *)lc_url->value;
    }
    else{
        return NULL;
    }
}

/**
 * @brief      Get location map URL.
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     location map URL, NULL otherwise.
 */
char *get_locationmap_url(struct VOEvent * voevent){
    struct VOEvent_Param *locmap_url = get_param_by_name(voevent, "LocationMap_URL");
    if(locmap_url){
        return (char *)locmap_url->value;
    }
    else{
        return NULL;
    }
}

/**
 * @brief      Get low channel index.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Low channel index if found, -1 otherwise.
 */
int get_low_channel_index(struct VOEvent *voevent){
    struct VOEvent_Param *low_chan_idx = get_param_by_name(voevent, "Lo_Chan_Index");
    if(low_chan_idx){
        return atoi((char *)low_chan_idx->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get high channel index.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     High channel index if found, -1 otherwise.
 */
int get_high_channel_index(struct VOEvent *voevent){
    struct VOEvent_Param *high_chan_idx = get_param_by_name(voevent, "Hi_Chan_Index");
    if(high_chan_idx){
        return atoi((char *)high_chan_idx->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get low channel energy.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Low channel energy if found, -1 otherwise.
 */
int get_low_channel_energy(struct VOEvent *voevent){
    struct VOEvent_Param *low_chan_energy = get_param_by_name(voevent, "Lo_Chan_Energy");
    if(low_chan_energy){
        return atoi((char *)low_chan_energy->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get high channel energy.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     High channel energy if found, -1 otherwise.
 */
int get_high_channel_energy(struct VOEvent *voevent){
    struct VOEvent_Param *high_chan_energy = get_param_by_name(voevent, "Hi_Chan_Energy");
    if(high_chan_energy){
        return atoi((char *)high_chan_energy->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get low energy range.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Low energy range if found, -1 otherwise.
 */
double get_low_energy_range(struct VOEvent *voevent){
    struct VOEvent_Param *low_energy = get_param_by_name(voevent, "Lo_Energy");
    if(low_energy){
        return atof((char *)low_energy->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get high energy range.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     High energy range if found, -1 otherwise.
 */
double get_high_energy_range(struct VOEvent *voevent){
    struct VOEvent_Param *high_energy = get_param_by_name(voevent, "Hi_Energy");
    if(high_energy){
        return atof((char *)high_energy->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get counts in a defined channel.
 *
 * @param      voevent       A VOEvent structure
 * @param      channel       Channel number
 *
 * @return     Number of counts if found, -1 otherwise.
 */
int get_counts_channel(struct VOEvent *voevent, int channel){
    if (channel <= 0 || channel > 4)
    {
        return -1;
    }

    struct VOEvent_Param *count_chan = NULL;

    switch(channel){
        case 1:
            count_chan = get_param_by_name(voevent, "Cnts_E1");
            if(count_chan){
                return atoi((char *)count_chan->value);
            }
            else{
                return -1;
            }
            break;
        case 2:
            count_chan = get_param_by_name(voevent, "Cnts_E2");
            if(count_chan){
                return atoi((char *)count_chan->value);
            }
            else{
                return -1;
            }
            break;
        case 3:
            count_chan = get_param_by_name(voevent, "Cnts_E3");
            if(count_chan){
                return atoi((char *)count_chan->value);
            }
            else{
                return -1;
            }
            break;
        case 4:
            count_chan = get_param_by_name(voevent, "Cnts_E4");
            if(count_chan){
                return atoi((char *)count_chan->value);
            }
            else{
                return -1;
            }
            break;
        default:
            break;
    }
    return -1;
}

/**
 * @brief      Get source temporal TS.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Source temporal TS if found, -1 otherwise.
 */
double get_source_temporal_ts(struct VOEvent *voevent){
    struct VOEvent_Param *src_t_ts = get_param_by_name(voevent, "Temp_Test_Stat");
    if(src_t_ts){
        return atof((char *)src_t_ts->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Get source image TS.
 *
 * @param      voevent       A VOEvent structure
 *
 * @return     Source image TS if found, -1 otherwise.
 */
double get_source_image_ts(struct VOEvent *voevent){
    struct VOEvent_Param *src_img_ts = get_param_by_name(voevent, "Image_Test_Stat");
    if(src_img_ts){
        return atof((char *)src_img_ts->value);
    }
    else{
        return -1;
    }
}
